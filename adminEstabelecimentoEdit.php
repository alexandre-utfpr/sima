<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['id',
'razao_social',
'cad_pro',
'municipio',
'bairro',
'cep',
'endereco',
'numero',
'telefone',
'uf',
'documento'
];

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} elseif (isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'estabelecimentos';

  $query = ShowQuery($tableName, $id);

  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $estabelecimento = $row;
    }
  } else {
    $errorMessage = $resourceNotFound;
  }

  CloseCon($conn);
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && !isset($_GET['id'])) {
  $errorMessage = $resourceNotFound;
}
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (ValidateRequired($_POST, $requiredFields)) {
    $triedToUpload = false;
    $fileUploaded = false;

    if ($_FILES['logo']['size'] != 0) {
      $triedToUpload = true;

      if (checkUpload($_FILES["logo"], array('jpeg', 'jpg', 'png'), 1500000)) {
        $uuid = substr(md5(rand()),0,10);
        $target_file = $target_dir . $uuid . '-' . basename($_FILES["logo"]["name"]);

        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
          $fileUploaded = true;
        } else {
          $errorMessage = $errorUploading;
          header("Location: adminEstabelecimentoEdit.php?id=".$_POST['id'], true, 301);
          return;
        }
      } else {
        $errorMessage = $invalidFileError;
        header("Location: adminEstabelecimentoEdit.php?id=".$_POST['id'], true, 301);
        return;
      }
    }

    if(!$triedToUpload || ($triedToUpload && !isset($errorMessage))) {
      $conn  = OpenCon();


      $id = trim($_POST['id']);
      $razao_social = trim($_POST['razao_social']);
      $cadPro = trim($_POST['cad_pro']);
      $municipio = trim($_POST['municipio']);
      $bairro = trim($_POST['bairro']);
      $endereco = trim($_POST['endereco']);
      $cep = trim($_POST['cep']);
      $numero = trim($_POST['numero']);
      $telefone = trim($_POST['telefone']);
      $uf = trim($_POST['uf']);
      $documento = trim($_POST['documento']);
      $data_entrada = trim($_POST['data_entrada']);

      $id = mysqli_real_escape_string($conn, $id);
      $razao_social = mysqli_real_escape_string($conn, $razao_social);
      $cadPro = mysqli_real_escape_string($conn, $cadPro);
      $municipio = mysqli_real_escape_string($conn, $municipio);
      $bairro = mysqli_real_escape_string($conn, $bairro);
      $endereco = mysqli_real_escape_string($conn, $endereco);
      $cep = mysqli_real_escape_string($conn, $cep);
      $numero = mysqli_real_escape_string($conn, $numero);
      $telefone = mysqli_real_escape_string($conn, $telefone);
      $uf = mysqli_real_escape_string($conn, $uf);
      $documento = mysqli_real_escape_string($conn, $documento);

      if ($fileUploaded) {
        $tableName = 'arquivos';
        $fields = 'url';
        $values = "'$target_file'";

        $query = InsertQuery($tableName, $fields, $values);
        $queryResult = mysqli_query($conn, $query);

        $fileId = mysqli_insert_id($conn);
      }

      $tableName = 'estabelecimentos';

      $fields = "razao_social = '$razao_social', ";

      if (isset($_POST['sim_poa']) && isset($_POST['classificacao'])) {
        $simPoa = trim($_POST['sim_poa']);
        $classificacao = trim($_POST['classificacao']);

        $simPoa = mysqli_real_escape_string($conn, $simPoa);
        $classificacao = mysqli_real_escape_string($conn, $classificacao);

        $fields.= "sim_poa = '$simPoa', ";
        $fields.= "classificacao = '$classificacao', ";
      }

      $fields.= "cad_pro = '$cadPro', ";
      $fields.= "bairro = '$bairro', ";
      $fields.= "municipio = '$municipio', ";
      $fields.= "bairro = '$bairro', ";
      $fields.= "endereco = '$endereco', ";
      $fields.= "numero = '$numero', ";
      $fields.= "telefone = '$telefone', ";
      $fields.= "cep = '$cep', ";
      $fields.= "uf = '$uf', ";
      $fields.= "documento = '$documento'";

      if (!empty($data_entrada)) {
        $data_entrada = DateTime::createFromFormat('d/m/Y', $data_entrada);
        $data_entrada = $data_entrada->format('Y-m-d');
        $fields .= ", data_entrada = '$data_entrada'";
      }

      if (isset($fileId)) {
        $fields .= ", arquivo_id = $fileId";
      }

      mysqli_begin_transaction($conn);

      $query = UpdateQuery($tableName, $fields, $id);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        mysqli_commit($conn);
        CloseCon($conn);
        header("Location: adminEstabelecimentoShow.php?id=".$id, true, 301);
      }
      else {
        mysqli_rollback($conn);
        $errorMessage = "$contactSupport";
        CloseCon($conn);
      }
    }
  }
  else {
    $errorMessage = $invalidFieldsError;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = @$estabelecimento['razao_social']; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <?php if(!isset($errorMessage) || ($errorMessage != $resourceNotFound)): ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Editar estabelecimento | <?php echo $estabelecimento['razao_social']; ?></h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="adminEstabelecimentoEdit.php" enctype="multipart/form-data">

                  <div class="form-group">
                    <p><span class="font-weight-bold text-success">Atenção! </span>Se o cadastro for pessoa física, preencher com os dados pessoais do produtor.</p>
                  </div>
                  <hr>

                  <input id="id" name="id"  value="<?php echo $estabelecimento['id']; ?>" type="hidden">

                  <div class="form-group">
                    <label for="razao_social" class="control-label required-field">Razão Social/Pessoa Física</label>
                    <input id="razao_social" name="razao_social" class="form-control form-control-user"
                    value="<?php echo $estabelecimento['razao_social']; ?>"
                    placeholder="Razão Social/Pessoa Física" type="text" pattern=".{1,40}" required>
                  </div>

                  <?php if($estabelecimento['aprovado'] == 1): ?>
                    <div class="form-group row">
                      <div class="col-3">
                        <label for="classificacao" class="control-label required-field">Classificação</label>
                        <input id="classificacao" name="classificacao" class="form-control form-control-user"
                        value="<?php echo $estabelecimento['classificacao']; ?>"
                        pattern=".{1,40}"
                        placeholder="Classificação" type="text" required>
                      </div>

                      <div class="col-3">
                        <label for="sim_poa" class="control-label required-field">SIM/POA</label>
                        <input id="sim_poa" name="sim_poa" class="form-control form-control-user"
                        value="<?php echo $estabelecimento['sim_poa']; ?>"
                        placeholder="SIM/POA" type="text" pattern=".{1,40}" required>
                      </div>

                      <div class="col-3">
                        <label for="data_entrada" class="control-label required-field">Data de entrada no SIM/POA</label>
                        <input onkeypress="return false;" type='text' data-language='pt-BR' required name="data_entrada"
                        value="<?php echo formatDate($estabelecimento['data_entrada']); ?>"
                        class="datepicker-here form-control" data-position="right top" />
                      </div>
                    </div>
                  <?php endif; ?>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="documento" class="control-label required-field">CNPJ ou CPF</label>
                      <input id="documento" name="documento" class="form-control form-control-user"
                      pattern="^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}" title="Digite o CNPJ ou CPF com máscara"
                      value="<?php echo $estabelecimento['documento']; ?>"
                      placeholder="nn.nnn.nnn/nnnn-nn ou nnn.nnn.nnn-nn" type="text" required>
                    </div>

                    <div class="col-4">
                      <label for="cad_pro" class="control-label required-field">CAD/PRO</label>
                      <input id="cad_pro" name="cad_pro" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['cad_pro']; ?>"
                      placeholder="CAD/PRO" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="municipio" class="control-label required-field">Municipio</label>
                      <input id="municipio" name="municipio" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['municipio']; ?>"
                      placeholder="Municipio" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-2">
                      <label for="uf" class="control-label required-field">UF</label>
                      <select name="uf" class="form-control form-control-user">
                        <?php foreach($ufs as $uf): ?>
                          <option value="<?php echo $uf; ?>"><?php echo $uf; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-9">
                      <label for="endereco" class="control-label required-field">Endereço</label>
                      <input id="endereco" name="endereco" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['endereco']; ?>"
                      placeholder="Endereço" type="text" pattern=".{1,80}" required>
                    </div>

                    <div class="col-3">
                      <label for="numero" class="control-label required-field">Número</label>
                      <input id="numero" name="numero" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['numero']; ?>"
                      placeholder="Número" type="number" min=1 required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-7">
                      <label for="bairro" class="control-label required-field">Bairro</label>
                      <input id="bairro" name="bairro" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['bairro']; ?>"
                      placeholder="Bairro" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-5">
                      <label for="cep" class="control-label required-field">CEP (Apenas números)</label>
                      <input id="cep" name="cep" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['cep']; ?>"
                      placeholder="CEP (Apenas números)" type="text" pattern="^[0-9]{8}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="telefone" class="control-label required-field">Telefone (Com DDD, apenas dígitos)</label>
                      <input id="telefone" name="telefone" class="form-control form-control-user"
                      value="<?php echo $estabelecimento['telefone']; ?>"
                      placeholder="Telefone (Com DDD, apenas dígitos)" type="text" pattern="^[0-9]{10, 11}" required>
                    </div>
                  </div>

                  <hr>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="logo" class="control-label required-field">Logo da empresa (JPEG ou PNG, máx. 1.5Mb)</label>
                      <input type="file" name="logo" id="logo">
                    </div>
                  </div>

                  <hr>

                  <a href="javascript:history.go(-1)" class="btn btn-info btn-sm float-right">Voltar</a>
                  <button type="submit" class="btn btn-success btn-sm">Salvar</button>
                </form>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <?php include("templates/footer.php");?>
  </div>
</div>
</body>
