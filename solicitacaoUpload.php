<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';
$requiredFields = ['protocolo', 'area', 'natureza', 'produto_nome'];

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields) && ($_FILES['document']['size'] != 0)) {

      if (checkUpload($_FILES["document"], array('pdf'), 5000000)) {
        $uuid = substr(md5(rand()),0,10);
        $target_file = $target_dir . $uuid . '-' . basename($_FILES["document"]["name"]);

        if (move_uploaded_file($_FILES["document"]["tmp_name"], $target_file)) {
          $conn  = OpenCon();

          $protocolo = trim($_POST['protocolo']);
          $area = trim($_POST['area']);
          $natureza = trim($_POST['natureza']);
          $produto_nome = trim($_POST['produto_nome']);

          $protocolo = mysqli_real_escape_string($conn, $protocolo);
          $area = mysqli_real_escape_string($conn, $area);
          $natureza = mysqli_real_escape_string($conn, $natureza);
          $produto_nome = mysqli_real_escape_string($conn, $produto_nome);

          $tableName = 'arquivos';
          $fields = 'url';
          $values = "'$target_file'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          $fileId = mysqli_insert_id($conn);

          $tableName = 'solicitacoes';
          $fields = 'protocolo, area, natureza, produto_nome, uploaded, arquivo_id';
          $values = "'$protocolo', '$area', '$natureza', '$produto_nome', 1, '$fileId'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          if ($queryResult) {
            CloseCon($conn);
            header("Location: solicitacaoIndex.php", true, 301);
          } else {
            $errorMessage = $contactSupport.mysqli_error($conn).$query;
            CloseCon($conn);
          }
        } else {
          $errorMessage = $errorUploading;
        }
      } else {
        $errorMessage = $invalidFileError;
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Adicionar solicitação'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Adicionar solicitação</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="solicitacaoUpload.php" enctype="multipart/form-data">

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="produto_nome" class="control-label required-field">Nome do produto</label>
                      <input id="produto_nome" name="produto_nome" class="form-control form-control-user"
                      placeholder="Nome do produto" type="text" pattern=".{1,100}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="protocolo" class="control-label required-field">Nº do protocolo</label>
                      <input id="protocolo" name="protocolo" class="form-control form-control-user"
                      placeholder="Nº do protocolo" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="area" class="control-label required-field">Área de atuação</label>
                      <input id="area" name="area" class="form-control form-control-user"
                      placeholder="Área de atuação" type="text" pattern=".{1,30}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="natureza" class="control-label required-field">Natureza da solicitação</label>
                      <select name="natureza" class="form-control form-control-user">
                        <option value="Registro">Registro</option>
                        <option value="Renovação do Registro">Renovação do Registro</option>
                        <option value="Alteração de composição do produto">Alteração de composição do produto</option>
                        <option value="Alteração do processo de fabricação">Alteração do processo de fabricação</option>
                      </select>
                    </div>
                  </div>
                  <hr>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="document" class="control-label required-field">Arquivo (PDF, máx. 5Mb)</label>
                      <input type="file" name="document" id="document" required>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Adicionar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
