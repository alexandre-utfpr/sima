<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';
$resourceOwner = null;

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $idCadastro = trim($_GET['id']);
    $idCadastro = mysqli_real_escape_string($conn, $idCadastro);

    $tableName = 'cadastros';

    $query = ShowQuery($tableName, $idCadastro);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $cadastro = $row;
      }

      $businessId = businessByUserId($cadastro['id']);
      $havesBusiness = (null != $businessId);
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  else {
    $errorMessage = $resourceNotFound;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Produtor'; include("templates/header.php"); ?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php"); ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php"); ?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800"><?php echo $title ?></h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success"><?php echo $cadastro['nome'] ?></h6>
                  </div>

                  <div class="card-body">
                    <p><span class="font-weight-bold">Nome: </span><?php echo $cadastro['nome'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">CPF: </span><?php echo $cadastro['cpf'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Endereço: </span><?php echo $cadastro['endereco'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Número: </span><?php echo $cadastro['numero'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Bairro: </span><?php echo $cadastro['bairro'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Municipio: </span><?php echo $cadastro['municipio'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">UF: </span><?php echo $cadastro['uf'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">CEP: </span><?php echo $cadastro['cep'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Telefone: </span><?php echo $cadastro['telefone'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Celular: </span><?php echo $cadastro['celular'] ?></p>
                    <hr>
                    <?php if($havesBusiness): ?>
                      <a href="adminEstabelecimentoShow.php?id=<?php echo $businessId; ?>">
                        <span class="text">Estabelecimento</span>
                      </a>
                      <hr>
                    <?php endif; ?>
                    <a href="adminCadastroEdit.php?id=<?php echo $cadastro['id']; ?>" class="btn btn-info btn-sm">
                      <span class="text">Editar</span>
                    </a>
                    <a href="adminCadastroDelete.php?id=<?php echo $cadastro['id']; ?>" class="btn btn-danger btn-sm float-right">
                      <span class="text">Deletar</span>
                    </a>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
