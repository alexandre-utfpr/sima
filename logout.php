<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

session_unset();
session_destroy();

header("Location: index.php", true, 301);
?>
