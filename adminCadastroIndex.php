<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();

  $tableName = 'cadastros';

  $where = "tipo = 'produtor'";

  $query = IndexQuery($tableName, $where);

  $queryResult = mysqli_query($conn, $query);

  if ($queryResult) {
    $cadastros = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($cadastros, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }

  CloseCon($conn);
}
?>


<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Produtores'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Produtores</h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Município</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Município</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php foreach($cadastros as &$cadastro): ?>
                            <tr>
                              <td><a href='adminCadastroShow.php?id=<?php echo $cadastro['id']; ?>'><?php echo $cadastro['nome']; ?></a></td>
                              <td><?php echo $cadastro['cpf']; ?></td>
                              <td><?php echo $cadastro['municipio']; ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                    <hr>
                    <a href="adminCadastroCreate.php" class="btn btn-info btn-sm">Cadastrar produtor</a>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
