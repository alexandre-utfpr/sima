<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();

  $tableName = 'estabelecimentos';

  $where = "aprovado = 1";

  $query = IndexQuery($tableName, $where);

  $queryResult = mysqli_query($conn, $query);

  if ($queryResult) {
    $estabelecimentos = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($estabelecimentos, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }

  CloseCon($conn);
}
?>

<?php if(isset($errorMessage)): ?>
  <?php include("templates/error.php");?>
  <?php else: ?>
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Razão Social | Nome</th>
            <th>Município</th>
            <th>Telefone</th>
            <th>CAD/PRO</th>
            <th>SIM/POA</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Razão Social | Nome</th>
            <th>Município</th>
            <th>Telefone</th>
            <th>CAD/PRO</th>
            <th>SIM/POA</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach($estabelecimentos as &$estabelecimento): ?>
            <tr>
              <td><a href='adminEstabelecimentoShow.php?id=<?php echo $estabelecimento['id']; ?>'><?php echo $estabelecimento['razao_social']; ?></a></td>
              <td><?php echo $estabelecimento['municipio']; ?></td>
              <td><?php echo $estabelecimento['telefone']; ?></td>
              <td><?php echo $estabelecimento['cad_pro']; ?></td>
              <td><?php echo @$estabelecimento['sim_poa']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <hr>
    <a href="adminEstabelecimentoCreate.php" class="btn btn-info btn-sm">Cadastrar estabelecimento</a>
  </div>
<?php endif; ?>
