<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Início'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Bem-vindo!</h1>

            <div class="card shadow mb-4">

              <?php if(!completeRegistration()): ?>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-success">Olá <?php echo sessionUserName(); ?>, seu cadastro ainda não está completo!</h6>
                </div>

                <div class="card-body">
                  <p>Olá <?php echo sessionUserName(); ?>! Cadastre seu estabelecimento ou pessoa física pelo menu lateral!</p>
                </div>
                <?php else: ?>
                  <?php if(approvedBusiness()): ?>
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-success">Olá <?php echo sessionUserName(); ?>, seu cadastro está completo!</h6>
                    </div>

                    <div class="card-body">
                      <p>Olá novamente <?php echo sessionUserName(); ?>! Seu cadastro está pronto, organize seus produtos e mantenha seu cadastro em dia.</p>
                    </div>
                    <?php else: ?>
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-success">Olá <?php echo sessionUserName(); ?>, seu cadastro está completo!</h6>
                      </div>

                      <div class="card-body">
                        <p>Olá novamente! Seu cadastro está completo, mas ainda <span class="font-weight-bold text-info"> não foi aprovado pela prefeitura!</span> Por enquanto, você ainda não pode cadastrar os seus produtos, mas pode visualizar e atulizar seus dados cadsastrados!</p>
                      </div>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>

            <?php if(approvedBusiness()): ?>
              <div class="container-fluid">
                <div class="col-12">
                  <div class="card shadow mb-4">
                   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success">Acesso rápido: Seus produtos</h6>
                  </div>

                  <div class="card-body">
                    <?php include('produtorProdutoIndexPartial.php'); ?>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
  </html>
