<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['ingrediente',
'porcentagem',
'quantidade',
'medida'
];

$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $produto = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      $resourceOwner = $estabelecimento['cadastro_id'];
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  else {
    $errorMessage = $resourceNotFound;
  }

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $conn  = OpenCon();

      $ingrediente = trim($_POST['ingrediente']);
      $quantidade = trim($_POST['quantidade']);
      $medida = trim($_POST['medida']);
      $porcentagem = trim($_POST['porcentagem']);
      $id = trim($_POST['id']);

      $ingrediente = mysqli_real_escape_string($conn, $ingrediente);
      $quantidade = mysqli_real_escape_string($conn, $quantidade);
      $medida = mysqli_real_escape_string($conn, $medida);
      $porcentagem = mysqli_real_escape_string($conn, $porcentagem);
      $id = mysqli_real_escape_string($conn, $id);

      $tableName = 'materias_primas';

      $fields = "ingrediente, quantidade, medida, porcentagem, produto_id";
      $values = "'$ingrediente', '$quantidade', '$medida', '$porcentagem', '$id'";

      $query = InsertQuery($tableName, $fields, $values);

      mysqli_begin_transaction($conn);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        mysqli_commit($conn);
        CloseCon($conn);
        header("Location: produtoShow.php?id=".$id, true, 301);
      }
      else {
        mysqli_rollback($conn);
        $errorMessage = $contactSupport.mysqli_error($conn).$query;
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = $produto['nome']; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Nova matéria-prima: <?php echo $produto['nome']; ?></h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="materiaPrimaCreate.php">
                  <input name="id" value="<?php echo $produto['id'] ?>" type="hidden">

                  <div class="form-group">
                    <label for="ingrediente" class="control-label required-field">Ingrediente / Aditivo (nº. registro no Órgão competente)</label>
                    <input id="ingrediente" name="ingrediente" class="form-control form-control-user"
                    placeholder="Ingrediente / Aditivo (nº. registro no Órgão competente)" type="text" pattern=".{1,40}" required>
                  </div>

                  <div class="form-group row">
                    <div class="col-3">
                      <label for="quantidade" class="control-label required-field">Quantidade</label>
                      <input id="quantidade" name="quantidade" class="form-control form-control-user"
                      placeholder="Quantidade" type="number" min="0.0" max="1000.0" step="0.0001" required>
                    </div>

                    <div class="col-2">
                      <label for="medida" class="control-label required-field">Unidade de medida</label>
                      <select name="medida" class="form-control form-control-user">
                        <option value="Kg">Kg</option>
                        <option value="L">L</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="porcentagem" class="control-label required-field">Porcentagem</label>
                    <h6 class='h6 text-danger'>Inserir, obrigatóriamente, no formato "0.0", no máximo 2 casas decimais</h6>
                    <div class='row'>
                      <div class="col-3">
                        <input id="porcentagem" name="porcentagem" class="form-control form-control-user"
                        placeholder="Porcentagem" type="number" min='0.0' max='100.0' step="0.01" pattern="^\d{1,3}\.\d{1,2}\$" required>
                      </div>
                    </div>
                  </div>

                  <hr>

                  <a href="javascript:history.go(-1)" class="btn btn-info btn-sm float-right">Voltar</a>
                  <button type="submit" class="btn btn-success btn-sm">Salvar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
