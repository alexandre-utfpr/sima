<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['email',
'password'
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (ValidateRequired($_POST, $requiredFields)) {
    $conn  = OpenCon();

    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    $email = mysqli_real_escape_string($conn, $email);
    $password = mysqli_real_escape_string($conn, $password);

    $tableName = 'usuarios';
    $where = "email = '$email' AND senha = '$password'";

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $user = $row;
      }

      $tableName = 'cadastros';

      $id = trim($user['cadastro_id']);
      $id = mysqli_real_escape_string($conn, $id);

      $query = ShowQuery($tableName, $id);

      $queryResult = mysqli_query($conn, $query);

      while($row = $queryResult->fetch_assoc()) {
        $cadastro = $row;
      }

      session_unset();
      session_destroy();
      session_start();

      $_SESSION["cadastro_id"] = $cadastro['id'];
      $_SESSION["type"] = $cadastro['tipo'];

      CloseCon($conn);
      if($_SESSION["type"] == 'admin') {
        header("Location: adminHome.php", true, 301);
        return;
      } else {
        header("Location: produtorHome.php", true, 301);
        return;
      }
    }
    else {
      $errorMessage = $userNotFound.mysqli_error($conn);
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/landingSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

        <?php include("templates/landingTopbar.php");?>

        <div class="row justify-content-center">

          <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg">
              <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                  <div class="col-lg-6 d-none d-lg-block">
                    <img src="assets/img/bg.jpg" alt="landing">
                  </div>
                  <div class="col-lg-6">
                    <div class="p-5">
                      <h4 class="h4 text-success mb-3"><i class="fas fa-fw fa-leaf"></i>Serviço de Inspeção Municipal Admin</h4>
                      <hr>
                      <?php if(isset($errorMessage)): ?>
                        <?php include("templates/error.php");?>
                      <?php endif; ?>
                      <div class="text-center">
                        <form id="loginForm" data-toggle="validator" class="user" role="form" method="post" action="login.php">
                          <div class="form-group">
                            <label for="email" class="control-label required-field">E-mail</label>
                            <input id="email" name="email" class="form-control form-control-user"
                            placeholder="E-mail" type="email" pattern=".{1,40}" required>
                          </div>

                          <div class="form-group">
                            <label for="password" class="control-label required-field">Senha</label>
                            <input id="password" name="password" class="form-control form-control-user"
                            placeholder="Senha" type="password" pattern=".{1,40}" required>
                          </div>

                          <button type="submit" class="btn btn-success btn-user form-control-user">Entrar</button>
                          <hr>
                          <div class="form-group">
                            <a href="newCadastro.php" class="text-success">
                              Cadastre-se
                            </a>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
