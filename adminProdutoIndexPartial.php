<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();
  $tableName = 'produtos';

  if (isset($_GET['id']) && $_GET['id'] != null) {
    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);
    $where = 'estabelecimento_id = '.$id;

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);
  } else {
    $query = ShowAllQuery($tableName);

    $queryResult = mysqli_query($conn, $query);
  }

  if ($queryResult) {
    $produtos = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($produtos, $row);
    }

    $tableName = 'estabelecimentos';

    $where = "aprovado = 1";

    $query = IndexQuery($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    $estabelecimentos = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($estabelecimentos, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }
  CloseCon($conn);
}
?>

<script src="assets/js/filtro_estabelecimento.js"></script>

<?php if(isset($errorMessage)): ?>
  <?php include("templates/error.php");?>
  <?php else: ?>
    <div class="card-body">
      <div class="row">
        <div class="col-4">
          <!-- Filtro definido por filtro_estabelecimento.js -->

          <select onchange="setEstabelecimento(this)" name="estabelecimentoSelect" id="estabelecimentoSelect" class="form-control form-control-user">
            <option value=""></option>
            <?php foreach($estabelecimentos as &$estabelecimento): ?>
              <option value="<?php echo $estabelecimento['id']; ?>"
                <?php if($estabelecimento['id'] == @$_GET['id']) { echo "selected"; } ?>
                >
                <?php echo $estabelecimento['razao_social']; ?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-5">
          <a id="indexURL" href="produtoIndex.php?id=<?php echo @$_GET['id'] ?>" class='btn btn-success btn-sm'>Filtrar</a>
        </div>
      </div>
      <hr>
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Marca / Nome Fantasia</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nome</th>
              <th>Marca / Nome Fantasia</th>
            </tr>
          </tfoot>
          <tbody>
            <?php foreach($produtos as &$produto): ?>
              <tr>
                <td><a href='produtoShow.php?id=<?php echo $produto['id']; ?>'><?php echo $produto['nome']; ?></a></td>
                <td><?php echo $produto['marca']; ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <hr>
      <a href="adminProdutoCreate.php?id=<?php echo @$_GET['id']; ?>" class="btn btn-info btn-sm">Cadastrar produto</a>
    </div>
  <?php endif; ?>
