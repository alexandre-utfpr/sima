<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['razao_social',
'cad_pro',
'municipio',
'bairro',
'cep',
'endereco',
'numero',
'telefone',
'uf',
'documento'
];

$pageRestriction = 'produtor';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $fileUploaded = false;

      if ($_FILES['logo']['size'] != 0) {
        if (checkUpload($_FILES["logo"], array('jpeg', 'jpg', 'png'), 1500000)) {
          $uuid = substr(md5(rand()),0,10);
          $target_file = $target_dir . $uuid . '-' . basename($_FILES["logo"]["name"]);

          if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
            $fileUploaded = true;
          } else {
            $errorMessage = $errorUploading;
            return;
          }
        } else {
          $errorMessage = $invalidFieldsError;
          return;
        }
      }

      $conn  = OpenCon();

      $razao_social = trim($_POST['razao_social']);
      $cadPro = trim($_POST['cad_pro']);
      $municipio = trim($_POST['municipio']);
      $bairro = trim($_POST['bairro']);
      $endereco = trim($_POST['endereco']);
      $cep = trim($_POST['cep']);
      $numero = trim($_POST['numero']);
      $telefone = trim($_POST['telefone']);
      $uf = trim($_POST['uf']);
      $documento = trim($_POST['documento']);

      $razao_social = mysqli_real_escape_string($conn, $razao_social);
      $cadPro = mysqli_real_escape_string($conn, $cadPro);
      $municipio = mysqli_real_escape_string($conn, $municipio);
      $bairro = mysqli_real_escape_string($conn, $bairro);
      $endereco = mysqli_real_escape_string($conn, $endereco);
      $cep = mysqli_real_escape_string($conn, $cep);
      $numero = mysqli_real_escape_string($conn, $numero);
      $telefone = mysqli_real_escape_string($conn, $telefone);
      $uf = mysqli_real_escape_string($conn, $uf);
      $documento = mysqli_real_escape_string($conn, $documento);
      $user_id = $_SESSION['cadastro_id'];

      if ($fileUploaded) {
        $tableName = 'arquivos';
        $fields = 'url';
        $values = "'$target_file'";

        $query = InsertQuery($tableName, $fields, $values);
        $queryResult = mysqli_query($conn, $query);

        $fileId = mysqli_insert_id($conn);
      }



      $tableName = 'estabelecimentos';
      $fields = 'cadastro_id, razao_social, cad_pro, municipio, bairro, endereco, cep, numero, telefone, uf, documento, aprovado';
      $values = "'$user_id', '$razao_social', '$cadPro', '$municipio', '$bairro', '$endereco', '$cep', ";
      $values .= "'$numero', '$telefone', '$uf', '$documento', FALSE";

      if (isset($fileId)) {
        $fields .= ', arquivo_id';
        $values .= ", '$fileId'";
      }

      $query = InsertQuery($tableName, $fields, $values);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        CloseCon($conn);
        header("Location: produtorEstabelecimentoShow.php", true, 301);
      } else {
        $errorMessage = $contactSupport.mysqli_error($conn);
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Novo Cadastro'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Cadastro de estabelecimento</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="produtorEstabelecimentoCreate.php" enctype="multipart/form-data">

                  <div class="form-group">
                    <p><span class="font-weight-bold text-success">Atenção! </span>Se seu cadastro for pessoa física, favor preencher com seus dados pessoais abaixo!</p>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label for="razao_social" class="control-label required-field">Razão Social/Pessoa Física</label>
                    <input id="razao_social" name="razao_social" class="form-control form-control-user"
                    placeholder="Razão Social/Pessoa Física" type="text" pattern=".{1,40}" required>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="documento" class="control-label required-field">CNPJ ou CPF</label>
                      <input id="documento" name="documento" class="form-control form-control-user"
                      pattern="^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}" title="Digite o CNPJ ou CPF com máscara"
                      placeholder="nn.nnn.nnn/nnnn-nn ou nnn.nnn.nnn-nn" type="text" required>
                    </div>

                    <div class="col-4">
                      <label for="cad_pro" class="control-label required-field">CAD/PRO</label>
                      <input id="cad_pro" name="cad_pro" class="form-control form-control-user"
                      placeholder="CAD/PRO" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="municipio" class="control-label required-field">Municipio</label>
                      <input id="municipio" name="municipio" class="form-control form-control-user"
                      placeholder="Municipio" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-2">
                      <label for="uf" class="control-label required-field">UF</label>
                      <select name="uf" class="form-control form-control-user">
                        <?php foreach($ufs as $uf){?>
                          <option value="<?php echo $uf; ?>"><?php echo $uf; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-9">
                      <label for="endereco" class="control-label required-field">Endereço</label>
                      <input id="endereco" name="endereco" class="form-control form-control-user"
                      placeholder="Endereço" type="text" pattern=".{1,80}" required>
                    </div>

                    <div class="col-3">
                      <label for="numero" class="control-label required-field">Número</label>
                      <input id="numero" name="numero" class="form-control form-control-user"
                      placeholder="Número" type="number" min=1 required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-7">
                      <label for="bairro" class="control-label required-field">Bairro</label>
                      <input id="bairro" name="bairro" class="form-control form-control-user"
                      placeholder="Bairro" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-5">
                      <label for="cep" class="control-label required-field">CEP (Apenas números)</label>
                      <input id="cep" name="cep" class="form-control form-control-user"
                      placeholder="CEP (Apenas números)" type="text" pattern="^[0-9]{8}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="telefone" class="control-label required-field">Telefone (Com DDD, apenas dígitos)</label>
                      <input id="telefone" name="telefone" class="form-control form-control-user"
                      placeholder="Telefone (Com DDD, apenas dígitos)" type="text" pattern="^[0-9]{10, 11}" required>
                    </div>
                  </div>

                  <hr>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="logo" class="control-label required-field">Logo da empresa (JPEG ou PNG, máx. 1.5Mb)</label>
                      <input type="file" name="logo" id="logo">
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
