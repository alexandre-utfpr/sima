<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';
require_once 'util/solicitacao_pdf_creator.php';

$requiredFields = ['protocolo',
'area',
'natureza',
'produto_id'
];

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  $conn  = OpenCon();

  $tableName = 'produtos';
  $query = ShowAllQuery($tableName);
  $queryResult = mysqli_query($conn, $query);

  $produtos = array();
  while($row = $queryResult->fetch_assoc()) {
    array_push($produtos, $row);
  }
  CloseCon($conn);

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $conn  = OpenCon();

      $protocolo = trim($_POST['protocolo']);
      $area = trim($_POST['area']);
      $natureza = trim($_POST['natureza']);
      $produto_id = trim($_POST['produto_id']);

      $protocolo = mysqli_real_escape_string($conn, $protocolo);
      $area = mysqli_real_escape_string($conn, $area);
      $natureza = mysqli_real_escape_string($conn, $natureza);
      $produto_id = mysqli_real_escape_string($conn, $produto_id);
      $additionalDocumentsTypes = additionalDocumentTypes();
      $documents = array();

      foreach($additionalDocumentsTypes as &$additionalDocumentType) {
        $tableName = 'arquivos_produtos';

        $document = new stdClass;
        $document->id = "";
        $document->field_id = $additionalDocumentType."_id";

        $where = "produto_id = '$produto_id' AND tipo = '$additionalDocumentType'";
        $query = ShowQueryWhere($tableName, $where);
        $queryResult = mysqli_query($conn, $query);

        if (mysqli_num_rows($queryResult) > 0) {
          while($row = $queryResult->fetch_assoc()) {
            $document->id = $row['arquivo_id'];
          }
        }

        array_push($documents, $document);
      }

      $tableName = 'produtos';

      $query = ShowQuery($tableName, $produto_id);
      $queryResult = mysqli_query($conn, $query);

      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }

      $produtoNome = $produto['nome'];
      $tableName = 'solicitacoes';

      $fields = "protocolo, area, natureza, produto_nome";
      $values = "'$protocolo', '$area', '$natureza', '$produtoNome'";
      foreach($documents as &$document) {
        $fields .= ", ".$document->field_id;
        if (empty($document->id)) {
          $values .= ", NULL";
        } else {
          $values .= ", '".$document->id."'";
        }
      }

      $query = InsertQuery($tableName, $fields, $values);
      $queryResult = mysqli_query($conn, $query);
      $solicitacaoId = mysqli_insert_id($conn);

      $arquivoId = generateSolicitacao($solicitacaoId);

      $fields = "arquivo_id = '$arquivoId'";

      $query = UpdateQuery($tableName, $fields, $solicitacaoId);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        CloseCon($conn);
        header("Location: solicitacaoIndex.php", true, 301);
      } else {
        $errorMessage = $contactSupport;
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Nova solicitação'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <?php if(empty($produtos)): ?>
                <div>
                  <div class="card bg-success text-white shadow">
                    <div class="card-body">
                      Nenhum produto cadastrado!
                    </div>
                  </div>
                </div>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success">Cadastro de nova solicitação</h6>
                  </div>

                  <div class="card-body">
                    <p><span class="font-weight-bold">Atenção! </span>A solicitação gerada será um arquivo estático no formato PDF. Não é possível editar uma solicitação, e os dados e arquivos atrelados serão os mesmos do produto selecionado no momento da criação da solicitação, e não serão afetados por edições futuras feitas no produto, bem como a deleção do mesmo.</p>
                    <form data-toggle="validator" role="form" method="post" action="solicitacaoCreate.php">
                      <h5 class='h5 text-success'>Produto</h5>
                      <hr>
                      <div class="form-group row">
                        <div class="col-5">
                          <select name="produto_id" class="form-control form-control-user">
                            <?php foreach($produtos as &$produto): ?>
                              <option value="<?php echo $produto['id']; ?>">
                                <?php echo $produto['nome']; ?>
                              </option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <br>

                      <h5 class='h5 text-success'>Solicitação</h5>
                      <hr>

                      <div class="form-group row">
                        <div class="col-5">
                          <label for="protocolo" class="control-label required-field">Nº do protocolo</label>
                          <input id="protocolo" name="protocolo" class="form-control form-control-user"
                          placeholder="Nº do protocolo" type="text" pattern=".{1,40}" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-5">
                          <label for="area" class="control-label required-field">Área de atuação</label>
                          <input id="area" name="area" class="form-control form-control-user"
                          placeholder="Área de atuação" type="text" pattern=".{1,30}" required>
                        </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-5">
                          <label for="natureza" class="control-label required-field">Natureza da solicitação</label>
                          <select name="natureza" class="form-control form-control-user">
                            <option value="Registro">Registro</option>
                            <option value="Renovação do Registro">Renovação do Registro</option>
                            <option value="Alteração de composição do produto">Alteração de composição do produto</option>
                            <option value="Alteração do processo de fabricação">Alteração do processo de fabricação</option>
                          </select>
                        </div>
                      </div>

                      <button type="submit" class="btn btn-primary">Registrar</button>
                    </form>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
