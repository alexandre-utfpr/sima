<p>
  <span class="font-weight-bold"><?php echo $additionalDocument->title ;?>:
    <?php if($solicitacao[$additionalDocument->type.'_id'] != null):?>
      <a class='text-info' href="<?php echo fileUrlFromId($solicitacao[$additionalDocument->type.'_id']); ?>" target="_blank">
        Visualizar
      </a>

      <span class="font-weight-bold"> | </span>

      <a class='text-danger' href="solicitacaoAdditionalFileDelete.php?id=<?php echo $solicitacao['id']; ?>&tipo=<?php echo $additionalDocument->type; ?>">
        Deletar
      </a>
    <?php else: ?>
      <a class='text-info' href="solicitacaoAdditionalFileCreate.php?id=<?php echo $solicitacao['id']; ?>&tipo=<?php echo $additionalDocument->type; ?>">
        Adicionar
      </a>
    <?php endif; ?>
  </span>
</p>
