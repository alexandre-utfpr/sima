<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
} else {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $tipo = trim($_GET['tipo']);

  $tipo = mysqli_real_escape_string($conn, $tipo);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'solicitacoes';

  $fields = $tipo."_id = NULL";

  $query = UpdateQuery($tableName, $fields, $id);
  $queryResult = mysqli_query($conn, $query);

  CloseCon($conn);

  header("Location: solicitacaoUploadedAdditionalDocuments.php?id=".$id, true, 301);
}
?>
