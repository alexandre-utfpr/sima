<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
} else {
  $conn = OpenCon();

  $query = ShowQuery('estabelecimentos', sessionBusinessId());

  $queryResult = mysqli_query($conn, $query);

  while($row = $queryResult->fetch_assoc()) {
    $estabelecimento = $row;
  }

  CloseCon($conn);
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Estabelecimento'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Estabelecimento</h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success"><?php echo $estabelecimento['razao_social'] ?></h6>
                  </div>

                  <div class="card-body">
                    <?php if(isset($estabelecimento['arquivo_id'])): ?>
                      <img src="<?php echo fileUrlFromId($estabelecimento['arquivo_id']);?>" alt="Logo"
                      style="max-height: 150px; height: auto; width: auto;"
                      >
                      <hr>
                    <?php endif; ?>
                    <p><span class="font-weight-bold">Nome: </span><?php echo $estabelecimento['razao_social'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">CNPJ/CPF: </span><?php echo $estabelecimento['documento'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">SIM/POA:</span>
                      <?php if(isset($estabelecimento['sim_poa'])): ?>
                        <?php echo $estabelecimento['sim_poa'] ?>
                        <?php else: ?>
                          <span class="text-info">Seu estabelecimento ainda não foi aprovado!</span>
                        <?php endif; ?>
                      </p>
                      <hr>
                      <p><span class="font-weight-bold">Classificação: </span><?php echo @$estabelecimento['classificacao'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">Endereço: </span><?php echo $estabelecimento['endereco'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">Número: </span><?php echo $estabelecimento['numero'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">Bairro: </span><?php echo $estabelecimento['bairro'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">Municipio: </span><?php echo $estabelecimento['municipio'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">UF: </span><?php echo $estabelecimento['uf'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">CEP: </span><?php echo $estabelecimento['cep'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">Telefone: </span><?php echo $estabelecimento['telefone'] ?></p>
                      <hr>
                      <p><span class="font-weight-bold">CAD/PRO: </span><?php echo $estabelecimento['cad_pro'] ?></p>
                      <hr>
                      <?php if(approvedBusiness()): ?>
                        <p><span class="font-weight-bold">Status: </span>Regular</p>
                        <?php else: ?>
                          <p><span class="font-weight-bold">Status: </span><span class="font-weight-bold text-info">Aguardando aprovação da prefeitura!</span></p>
                        <?php endif; ?>
                        <a href="produtorEstabelecimentoEdit.php" class="btn btn-info btn-sm">
                          <span class="text">Editar</span>
                        </a>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
            <?php include("templates/footer.php");?>
          </div>
        </div>
      </body>
