<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();

  $tableName = 'solicitacoes';

  $query = ShowAllQuery($tableName);
  $queryResult = mysqli_query($conn, $query);

  if ($queryResult) {
    $solicitacoes = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($solicitacoes, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }

  CloseCon($conn);
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Solicitações'; include("templates/header.php");?>
<script src="assets/js/delete_solicitacao.js"></script>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Solicitações</h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Visualizar</th>
                            <th>Protocolo</th>
                            <th>Natureza</th>
                            <th>Produto</th>
                            <th>Documentos adicionais</th>
                            <th>Deletar</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>Visualizar</th>
                            <th>Protocolo</th>
                            <th>Natureza</th>
                            <th>Produto</th>
                            <th>Documentos adicionais</th>
                            <th>Deletar</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php foreach($solicitacoes as &$solicitacao): ?>
                            <tr>
                              <td><a href="<?php echo fileUrlFromId($solicitacao['arquivo_id']); ?>" target="_blank" class="font-weight-bold text-success">Visualizar</a></td>
                              <td><?php echo $solicitacao['protocolo']; ?></td>
                              <td><?php echo $solicitacao['area']; ?></td>
                              <td><?php echo $solicitacao['produto_nome']; ?></td>
                              <?php if($solicitacao['uploaded']): ?>
                                <td><a href="solicitacaoUploadedAdditionalDocuments.php?id=<?php echo $solicitacao['id']; ?>" target="_blank" class="font-weight-bold text-info">Documentos adicionais</a></td>
                                <?php else: ?>
                                  <td><a href="solicitacaoAdditionalDocuments.php?id=<?php echo $solicitacao['id']; ?>" target="_blank" class="font-weight-bold text-info">Documentos adicionais</a></td>
                                <?php endif; ?>
                                <td><p id="<?php echo $solicitacao['id']; ?>" href=" " class="font-weight-bold text-danger" onclick="deleteSolicitacao(this)" style="cursor: pointer;">
                                Deletar</p>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                    <hr>
                    <a href="solicitacaoCreate.php" class="btn btn-info btn-sm">Nova solicitação</a>
                    <a href="solicitacaoUpload.php" class="btn btn-success btn-sm">Enviar solicitação digitalizada</a>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
