<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['nome',
'municipio',
'bairro',
'cep',
'endereco',
'numero',
'telefone',
'uf',
'documento',
'produto_id'
];

$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $produto = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      $resourceOwner = $estabelecimento['cadastro_id'];
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  else {
    $errorMessage = $resourceNotFound;
  }

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $conn  = OpenCon();

      $nome = trim($_POST['nome']);
      $documento = trim($_POST['documento']);
      $endereco = trim($_POST['endereco']);
      $numero = trim($_POST['numero']);
      $uf = trim($_POST['uf']);
      $municipio = trim($_POST['municipio']);
      $bairro = trim($_POST['bairro']);
      $cep = trim($_POST['cep']);
      $telefone = trim($_POST['telefone']);
      $produto_id = trim($_POST['produto_id']);

      $nome = mysqli_real_escape_string($conn, $nome);
      $documento = mysqli_real_escape_string($conn, $documento);
      $endereco = mysqli_real_escape_string($conn, $endereco);
      $numero = mysqli_real_escape_string($conn, $numero);
      $uf = mysqli_real_escape_string($conn, $uf);
      $municipio = mysqli_real_escape_string($conn, $municipio);
      $bairro = mysqli_real_escape_string($conn, $bairro);
      $cep = mysqli_real_escape_string($conn, $cep);
      $telefone = mysqli_real_escape_string($conn, $telefone);
      $produto_id = mysqli_real_escape_string($conn, $produto_id);

      $tableName = 'marca_terceiros';
      $fields = 'nome, documento, endereco, numero, uf, municipio, bairro, cep, telefone, produto_id';
      $values = "'$nome', '$documento', '$endereco', '$numero', '$uf', ";
      $values .= "'$municipio', '$bairro', '$cep', '$telefone', '$produto_id'";

      $query = InsertQuery($tableName, $fields, $values);

      mysqli_begin_transaction($conn);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        mysqli_commit($conn);
        CloseCon($conn);
        header("Location: produtoShow.php?id=".$produto_id, true, 301);
      }
      else {
        mysqli_rollback($conn);
        $errorMessage = $contactSupport.mysqli_error($conn).$query;
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Cadastro de marca de terceiros'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Cadastro de marca de terceiros</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="marcaTerceiroCreate.php">
                  <input name="produto_id" value="<?php echo $produto['id'] ?>" type="hidden">

                  <div class="form-group">
                    <label for="nome" class="control-label required-field">Razão Social/Pessoa Física</label>
                    <input id="nome" name="nome" class="form-control form-control-user"
                    placeholder="Razão Social/Pessoa Física" type="text" pattern=".{1,40}" required>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="documento" class="control-label required-field">CNPJ ou CPF</label>
                      <input id="documento" name="documento" class="form-control form-control-user"
                      pattern="^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}" title="Digite o CNPJ ou CPF com máscara"
                      placeholder="nn.nnn.nnn/nnnn-nn ou nnn.nnn.nnn-nn" type="text" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="municipio" class="control-label required-field">Municipio</label>
                      <input id="municipio" name="municipio" class="form-control form-control-user"
                      placeholder="Municipio" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-2">
                      <label for="uf" class="control-label required-field">UF</label>
                      <select name="uf" class="form-control form-control-user">
                        <?php foreach($ufs as $uf){?>
                          <option value="<?php echo $uf; ?>"><?php echo $uf; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-9">
                      <label for="endereco" class="control-label required-field">Endereço</label>
                      <input id="endereco" name="endereco" class="form-control form-control-user"
                      placeholder="Endereço" type="text" pattern=".{1,80}" required>
                    </div>

                    <div class="col-3">
                      <label for="numero" class="control-label required-field">Número</label>
                      <input id="numero" name="numero" class="form-control form-control-user"
                      placeholder="Número" type="number" min=1 required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-7">
                      <label for="bairro" class="control-label required-field">Bairro</label>
                      <input id="bairro" name="bairro" class="form-control form-control-user"
                      placeholder="Bairro" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-5">
                      <label for="cep" class="control-label required-field">CEP (Apenas números)</label>
                      <input id="cep" name="cep" class="form-control form-control-user"
                      placeholder="CEP (Apenas números)" type="text" pattern="^[0-9]{8}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="telefone" class="control-label required-field">Telefone (Com DDD, apenas dígitos)</label>
                      <input id="telefone" name="telefone" class="form-control form-control-user"
                      placeholder="Telefone (Com DDD, apenas dígitos)" type="text" pattern="^[0-9]{10, 11}" required>
                    </div>
                  </div>

                  <hr>

                  <a href="javascript:history.go(-1)" class="btn btn-info btn-sm float-right">Voltar</a>
                  <button type="submit" class="btn btn-success btn-sm">Salvar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
