<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = [
'userEmail',
'userPassword',
'userPasswordConfirmation',
'cadastroNome',
'cadastroCpf',
'cadastroMunicipio',
'cadastroBairro',
'cadastroEndereco',
'cadastroNumero',
'cadastroTelefone',
'cadastroCelular',
'cadastroUf',
'cadastroCep',
'cadastroTelefone'
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (ValidateRequired($_POST, $requiredFields)) {
    $conn  = OpenCon();

    $userEmail = trim($_POST['userEmail']);
    $userEmail = mysqli_real_escape_string($conn, $userEmail);

    $tableName = 'usuarios';
    $where = "email = '$userEmail'";

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    $duplicates =  (mysqli_num_rows($queryResult) > 0);

    if ($_POST['userPassword'] != $_POST['userPasswordConfirmation']) {
      $errorMessage = $invalidPassword;
      CloseCon($conn);
    }
    elseif($duplicates) {
      $errorMessage = $duplicateUser;
      CloseCon($conn);
    } else {
      $cadastroNome = trim($_POST['cadastroNome']);
      $cadastroCpf = trim($_POST['cadastroCpf']);
      $cadastroMunicipio = trim($_POST['cadastroMunicipio']);
      $cadastroBairro = trim($_POST['cadastroBairro']);
      $cadastroEndereco = trim($_POST['cadastroEndereco']);
      $cadastroNumero = trim($_POST['cadastroNumero']);
      $cadastroTelefone = trim($_POST['cadastroTelefone']);
      $cadastroCelular = trim($_POST['cadastroCelular']);
      $cadastroUf = trim($_POST['cadastroUf']);
      $cadastroCep = trim($_POST['cadastroCep']);

      $cadastroNome = mysqli_real_escape_string($conn, $cadastroNome);
      $cadastroCpf = mysqli_real_escape_string($conn, $cadastroCpf);
      $cadastroMunicipio = mysqli_real_escape_string($conn, $cadastroMunicipio);
      $cadastroBairro = mysqli_real_escape_string($conn, $cadastroBairro);
      $cadastroEndereco = mysqli_real_escape_string($conn, $cadastroEndereco);
      $cadastroNumero = mysqli_real_escape_string($conn, $cadastroNumero);
      $cadastroTelefone = mysqli_real_escape_string($conn, $cadastroTelefone);
      $cadastroCelular = mysqli_real_escape_string($conn, $cadastroCelular);
      $cadastroUf = mysqli_real_escape_string($conn, $cadastroUf);
      $cadastroCep = mysqli_real_escape_string($conn, $cadastroCep);

      $tableName = 'cadastros';
      $fields = 'tipo, nome, cpf, municipio, bairro, endereco, numero, telefone, celular, uf, cep';
      $values = "'produtor', '$cadastroNome', '$cadastroCpf', '$cadastroMunicipio', '$cadastroBairro', '$cadastroEndereco', ";
      $values .= "'$cadastroNumero', '$cadastroTelefone', '$cadastroCelular', '$cadastroUf', '$cadastroCep'";

      $query = InsertQuery($tableName, $fields, $values);

      mysqli_begin_transaction($conn);

      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        $id = mysqli_insert_id($conn);

        $userPassword = trim($_POST['userPassword']);
        $userPassword = mysqli_real_escape_string($conn, $userPassword);

        $tableName = 'usuarios';
        $fields = 'email, senha, cadastro_id';
        $values = "'$userEmail', '$userPassword', $id";

        $query = InsertQuery($tableName, $fields, $values);
        $queryResult = mysqli_query($conn, $query);

        if ($queryResult) {
          mysqli_commit($conn);
          session_start();
          $_SESSION["cadastro_id"] = $id;
          $_SESSION["type"] = 'produtor';

          CloseCon($conn);
          header("Location: produtorHome.php", true, 301);
        }
        else {
          $errorMessage = $contactSupport;

          mysqli_rollback($conn);
          CloseCon($conn);
        }
      } else {
        $errorMessage = $contactSupport;

        mysqli_rollback($conn);
        CloseCon($conn);
      }
    }
  }
  else {
    $errorMessage = $invalidFieldsError;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/landingSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

        <?php include("templates/landingTopbar.php");?>
        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Cadastro de novo produtor</h6>
              </div>

              <div class="card-body">
                <form id="cadastroCreateForm" data-toggle="validator" role="form" method="post" action="newCadastro.php">

                  <div class="form-group">
                    <label for="cadastroNome" class="control-label required-field">Nome completo</label>
                    <input id="cadastroNome" name="cadastroNome" class="form-control form-control-user"
                    placeholder="Nome completo" type="text" pattern=".{1,40}" required>
                  </div>

                  <div class="form-group row">
                    <div class="col-3">
                      <label for="cadastroCpf" class="control-label required-field">CPF</label>
                      <input id="cadastroCpf" name="cadastroCpf" class="form-control form-control-user"
                      pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" title="Digite o CPF no formato nnn.nnn.nnn-nn"
                      placeholder="nnn.nnn.nnn-nn" type="text" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="cadastroMunicipio" class="control-label required-field">Municipio</label>
                      <input id="cadastroMunicipio" name="cadastroMunicipio" class="form-control form-control-user"
                      placeholder="Municipio" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-2">
                      <label for="cadastroUf" class="control-label required-field">UF</label>
                      <select name="cadastroUf" class="form-control form-control-user">
                        <?php foreach($ufs as $uf){?>
                          <option value="<?php echo $uf; ?>"><?php echo $uf; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-9">
                      <label for="cadastroEndereco" class="control-label required-field">Endereço</label>
                      <input id="cadastroEndereco" name="cadastroEndereco" class="form-control form-control-user"
                      placeholder="Endereço" type="text" pattern=".{1,80}" required>
                    </div>

                    <div class="col-3">
                      <label for="cadastroNumero" class="control-label required-field">Número</label>
                      <input id="cadastroNumero" name="cadastroNumero" class="form-control form-control-user"
                      placeholder="Número" type="number" min=1 required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-7">
                      <label for="cadastroBairro" class="control-label required-field">Bairro</label>
                      <input id="cadastroBairro" name="cadastroBairro" class="form-control form-control-user"
                      placeholder="Bairro" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-5">
                      <label for="cadastroCep" class="control-label required-field">CEP (Apenas números)</label>
                      <input id="cadastroCep" name="cadastroCep" class="form-control form-control-user"
                      placeholder="CEP (Apenas números)" type="text" pattern="^[0-9]{8}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="cadastroTelefone" class="control-label required-field">Telefone</label>
                      <input id="cadastroTelefone" name="cadastroTelefone" class="form-control form-control-user"
                      placeholder="Telefone (Com DDD, apenas dígitos)" type="number" pattern="^[0-9]{10, 11}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="cadastroCelular" class="control-label required-field">Celular</label>
                      <input id="cadastroCelular" name="cadastroCelular" class="form-control form-control-user"
                      placeholder="Celular (Com DDD, apenas dígitos)" type="number" pattern="^[0-9]{11}" required>
                    </div>
                  </div>

                  <hr>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="userEmail" class="control-label required-field">E-mail</label>
                      <input id="userEmail" name="userEmail" class="form-control form-control-user"
                      placeholder="E-mail" type="email" pattern='.{1,50}' required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="userPassword" class="control-label required-field">Senha (min. 8 caracteres)</label>
                      <input id="userPassword" name="userPassword" class="form-control form-control-user"
                      placeholder="Senha" type="password" pattern='.{8,50}' required>
                    </div>
                    <div class="col-6">
                      <label for="userPasswordConfirmation" class="control-label required-field">Confirmação da senha (min. 8 caracteres)</label>
                      <input id="userPasswordConfirmation" name="userPasswordConfirmation" class="form-control form-control-user"
                      placeholder="Confirmação da senha" type="password" pattern='.{8,50}' required>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
