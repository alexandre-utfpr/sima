<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';


$conn  = OpenCon();

$tableName = 'legislacoes';

$query = IndexQuery($tableName, null);

$queryResult = mysqli_query($conn, $query);

$legislacoes = array();

if ($queryResult) {
  while($row = $queryResult->fetch_assoc()) {
    array_push($legislacoes, $row);
  }
}

CloseCon($conn);
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/landingSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/landingTopbar.php");?>

        <div class="row justify-content-center">

          <div class="col-xl-6 col-lg-8 col-md-5">

            <div class="card o-hidden border-0 shadow-lg">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Legislações e documentos</h6>
              </div>
              <div class="card-body p-0">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-5">

                      <?php foreach($legislacoes as &$legislacao): ?>
                        <a href="<?php echo fileUrlFromId($legislacao['arquivo_id']); ?>" target="_blank">
                          <div class="col-12">
                            <div class="card border-left-success shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="h3 mb-0 font-weight-bold text-success">
                                      <?php echo $legislacao['titulo']; ?>
                                    </div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fas fa-scroll fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        <hr>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
