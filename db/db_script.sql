CREATE DATABASE IF NOT EXISTS sima;

USE sima;

CREATE TABLE solicitacoes (
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  protocolo CHAR(50),
  area CHAR(50),
  natureza CHAR(50),
  produto_nome CHAR(100),
  arquivo_id INT,
  responsabilidade_marca_id INT,
  autorizacao_uso_id INT,
  registro_marca_id INT,
  sem_regulamentacao_tecnica_id INT,
  atendimento_rtiq_id INT,
  croqui_id INT,
  fichas_tecnicas_id INT,
  rotulo_id INT,
  uploaded BOOLEAN,
  producao_especfica_id INT
  );

CREATE TABLE produtos(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  nome CHAR(100),
  marca CHAR(100),
  registro CHAR(50),
  sisbi BOOLEAN,
  municipal BOOLEAN,
  tipo_rotulo INT,
  tipo_primaria CHAR(50),
  tipo_secundaria CHAR(50),
  quantidade_primaria CHAR(50),
  peso_embalagem CHAR(50),
  quantidade_secundaria CHAR(50),
  local_fabricacao CHAR(50),
  tipo_aplicacao CHAR(50),
  forma_indicacao CHAR(50),
  descricao_lote TEXT,
  gluten BOOLEAN,
  tartrazina BOOLEAN,
  fenilalanina BOOLEAN,
  transgenicos BOOLEAN,
  corante BOOLEAN,
  aromatizante BOOLEAN,
  processo_fabricacao TEXT,
  natureza TEXT,
  sistema_rotulagem TEXT,
  local_armazenamento CHAR(50),
  forma_armazenamento CHAR(50),
  temperatura_armazenamento INT,
  temperatura_produto INT,
  meio_transporte CHAR(50),
  condicao_transporte CHAR(50),
  forma_acondicionamento CHAR(50),
  temperatura_transporte INT,
  estabelecimento_id INT
  );

CREATE TABLE marca_terceiros(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  nome CHAR(50),
  documento CHAR(18),
  endereco CHAR(80),
  numero INT,
  uf CHAR(2),
  municipio CHAR(50),
  bairro CHAR(50),
  cep INT,
  telefone CHAR(13),
  produto_id INT
);

CREATE TABLE arquivos_produtos(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  tipo CHAR(50),
  arquivo_id INT,
  produto_id INT
);

CREATE TABLE materias_primas(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  ingrediente CHAR(100),
  quantidade FLOAT(9,5),
  medida CHAR(2),
  porcentagem FLOAT(5,2),
  produto_id INT
  );

CREATE TABLE estabelecimentos(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  razao_social CHAR(50),
  sim_poa CHAR(50),
  data_entrada DATE,
  classificacao CHAR(50),
  municipio CHAR(50),
  bairro CHAR(50),
  endereco CHAR(80),
  numero INT,
  telefone CHAR(13),
  uf CHAR(2),
  documento CHAR(18),
  cad_pro CHAR(50),
  cep INT,
  arquivo_id INT,
  cadastro_id INT,
  aprovado BOOLEAN
);

CREATE TABLE usuarios(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  email CHAR(50),
  senha CHAR(50),
  cadastro_id INT
);

CREATE TABLE cadastros(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  tipo CHAR(50),
  cpf CHAR(14),
  documento CHAR(18),
  nome CHAR(50),
  uf CHAR(2),
  municipio CHAR(50),
  bairro CHAR(50),
  endereco CHAR(80),
  cep INT,
  numero INT,
  telefone CHAR(13),
  celular CHAR(14)
);

CREATE TABLE legislacoes(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  titulo CHAR(100),
  arquivo_id INT
);

CREATE TABLE arquivos(
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  url CHAR(100)
);

ALTER TABLE produtos ADD CONSTRAINT fk_produto_estabelecimento_id FOREIGN KEY (estabelecimento_id) REFERENCES estabelecimentos(id) ON DELETE CASCADE;
ALTER TABLE materias_primas ADD CONSTRAINT fk_materia_prima_produto_id FOREIGN KEY (produto_id) REFERENCES produtos(id) ON DELETE CASCADE;
ALTER TABLE marca_terceiros ADD CONSTRAINT fk_marca_terceiros_produto_id FOREIGN KEY (produto_id) REFERENCES produtos(id) ON DELETE CASCADE;
ALTER TABLE estabelecimentos ADD CONSTRAINT fk_estabelecimeno_cadastro_id FOREIGN KEY (cadastro_id) REFERENCES cadastros(id) ON DELETE CASCADE;
ALTER TABLE usuarios ADD CONSTRAINT fk_usuario_cadastro_id FOREIGN KEY (cadastro_id) REFERENCES cadastros(id) ON DELETE CASCADE;
