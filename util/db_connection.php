<?php

  //Cria uma conexão com o DB
  function OpenCon() {
    $dbhost = "localhost";
    $dbuser = "sima";
    $dbpass = "sima";
    $db = "sima";
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Erro de conexão: %s\n". $conn -> error);

    return $conn;
  }

  // Fecha a conexão com o db
  function CloseCon($conn) {
    $conn->close();
  }

  //Funções para criações de queries, aceitar uma tabela, valores e campos, e ids quanto pertinente
  function InsertQuery($tableName, $fields, $values) {
    return "INSERT INTO $tableName ($fields) VALUES ($values);";
  }

  function UpdateQuery($tableName, $fields, $id) {
    return "UPDATE $tableName SET $fields WHERE id = $id;";
  }

  function UpdateQueryWhere($tableName, $fields, $where) {
    return "UPDATE $tableName SET $fields WHERE $where;";
  }

  function ShowQuery($tableName, $id) {
    return "SELECT * FROM $tableName WHERE id = $id;";
  }

  function ShowQueryWhere($tableName, $where) {
    return "SELECT * FROM $tableName WHERE $where;";
  }

  function ShowAllQuery($tableName) {
    return "SELECT * FROM $tableName;";
  }

  function IndexQuery($tableName, $where) {
    $query = "SELECT * FROM $tableName ";

    if (isset($where)) {
      $query .= "WHERE $where";
    }

    $query .= " ORDER BY id ASC;";

    return $query;
  }

  function DeleteQuery($tableName, $id) {
    return "DELETE FROM $tableName WHERE id = $id;";
  }
?>
