<?php
  require_once 'lib/fpdf/fpdf.php';
  require_once 'util/strings.php';
  require_once 'util/db_connection.php';
  require_once 'util/page_utils.php';
  class PDF extends FPDF
  {
    // Page header
    function Header()
    {
      global $solicitacao;

      $this->SetFont('Times','',12);
      $this->Cell(90,50,$this->Image('assets/img/simlogo.jpg',12,12, 85, 35),1,0,'C');

      $this->SetY($this->GetY() + 45);
      $this->SetX($this->GetX() + 25);
      $this->Cell(0,0,'GUARAPUAVA - PR');

      $this->SetX(100);
      $this->SetY(10);
      $this->Cell(100);

      $this->SetFont('Times','B',9);
      $this->Cell(0,10, 'Nº PROTOCOLO', 'TRL',2,'C');

      $this->Cell(0,20, $solicitacao['protocolo'], 'LR',2,'C');

      $this->SetFont('Times','',8);
      $this->Cell(45,10, 'ÁREA DE ATUAÇÃO', 'TLR',0,'C');

      $this->Cell(45,10, 'NATUREZA DA SOLICITAÇÃO', 'TR',1,'C');

      $this->SetX(100);
      $this->SetY(50);
      $this->Cell(100);

      $this->Cell(45,10, $solicitacao['area'], 'LRB',0,'C');

      $this->Cell(45,10, $solicitacao['natureza'], 'RB',1,'C');
      $this->Cell(0,5, '', 0,1,'C');
    }

    // Page footer
    function Footer()
    {
      $this->SetY(-44);
      $this->SetFont('Times','B',8);
      $this->Cell(0,10, 'AUTENTICAÇÃO',0,0,'L');
      $this->Ln();
      $this->SetFont('Times','',8);
      $this->Cell(40,5, 'DATA','TLR',0,'L');
      $this->Cell(75,5, 'CARIMBO E ASS. DO RESPONSÁVEL LEGAL','TR',0,'L');
      $this->Cell(0,5, 'CARIMBO E ASS. DO RESPONSÁVEL TÉCNICO','TR',0,'L');
      $this->Ln();
      $this->Cell(40,10,'','LRB',0,'L');
      $this->Cell(75,10,'','BR',0,'L');
      $this->Cell(0,10,'','BR',1,'L');
      $this->SetFont('Times','B',12);
      $this->Cell(0,10,$this->PageNo(),0,1,'R');
    }
  }

  function identificacao($pdf) {
    global $estabelecimento, $produto;

    $dataEntrada = empty($estabelecimento['data_entrada']) ? '' : formatDate($estabelecimento['data_entrada']);

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '1- IDENTIFICAÇÃO',0,1,'L');

    $pdf->SetFont('Times','',7);
    $pdf->Cell(65,3, '1.1 Nº SIM/POA','TLR',0,'L');
    $pdf->Cell(65,3, '1.2 Nº DE REGISTRO','TR',0,'L');
    $pdf->Cell(25,3, '1.3 DATA ENTRADA','TR',0,'L');
    $pdf->Cell(0,3, '1.4 COMERCIALIZAÇÃO','TR',1,'L');

    $pdf->Cell(65,3, 'DO ESTABELECIMENTO','L',0,'L');
    $pdf->Cell(65,3, 'DO PRODUTO','L', 0,'L');
    $pdf->Cell(25,3, 'NO SIM/POA','L',0,'L');
    $pdf->Cell(0,3, "1- SISBI (".check($produto['sisbi']).")",'LR',1,'L');

    $pdf->Cell(65,5, $estabelecimento['sim_poa'],'LB','L');
    $pdf->Cell(65,5, $produto['registro'], 'LB', 0,'L');
    $pdf->Cell(25,5, $dataEntrada,'LB',0,'L');
    $pdf->Cell(0,5, "2- MUNICIPAL (".check($produto['municipal']).")",'LBR',1,'L');
  }

  function peticao($pdf) {
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '2 - PETIÇÃO',0,1,'L');

    $pdf->Cell(0,5, 'Ao Serviço de Inspeção Municipal /Produtos de Origem Animal - SIP/POA','TLR',1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,3, 'O Estabelecimento abaixo qualificado, através do seu representante legal e do seu responsável técnico, requer o atendimento da solicitação especificada neste','LR',1,'L');
    $pdf->Cell(0,3, 'documento.','LRB',1,'L');
  }

  function identificacaoEstabelecimento($pdf) {
    global $estabelecimento;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '3 - IDENTIFICAÇÃO DO ESTABELECIMENTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(50,5, '3.1 RAZÃO SOCIAL/PESSOA FISICA:','TL',0,'L');
    $pdf->Cell(0,5, $estabelecimento['razao_social'], 'TR',1,'L');

    $pdf->Cell(38,5, '3.2 CNPJ ou CPF e CAD/PRO:','TL',0,'L');
    $pdf->Cell(35,5, $estabelecimento['documento'],'TR',0,'L');

    $pdf->Cell(60,5, '3.3 CLASSIFICAÇÃO DO ESTABELECIMENTO:','TL',0,'L');
    $pdf->Cell(0,5, '','TR',1,'L');

    $pdf->Cell(73,5, $estabelecimento['cad_pro'],'LB',0,'L');
    $pdf->Cell(0,5, $estabelecimento['classificacao'],'LBR',1,'L');
  }

  function marcaTerceiros($pdf) {
    global $marcaTerceiro;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '4 - IDENTIFICAÇÃO DE MARCAS DE TERCEIROS',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(48,5, '4.1 RAZÃO SOCIAL/PESSOA FISICA:','TL',0,'L');
    $pdf->Cell(87,5, @$marcaTerceiro['nome'],'TR',0,'L');

    $pdf->Cell(20,5, '4.2 CPF/CNPJ:','TL',0,'L');
    $pdf->Cell(0,5, @$marcaTerceiro['documento'],'TR',1,'L');

    $pdf->Cell(55,4, '4.3 ENDEREÇO/TELEFONE DE CONTATO:','TL',0,'L');
    $pdf->Cell(0,4, @$marcaTerceiro['telefone'],'TR',1,'L');
    $pdf->Cell(0,4, @$marcaTerceiro['endereco'],'LRB',1,'L');

    $pdf->Cell(16,5, '4.4 BAIRRO:','LB',0,'L');
    $pdf->Cell(53,5, @$marcaTerceiro['bairro'],'B',0,'L');
    $pdf->Cell(11,5, '4.5 CEP:','LB',0,'L');
    $pdf->Cell(15,5, @$marcaTerceiro['cep'],'B',0,'L');
    $pdf->Cell(22,5, '4.6 MUNICÍPIO:','LB',0,'L');
    $pdf->Cell(55,5, @$marcaTerceiro['municipio'],'B',0,'L');
    $pdf->Cell(9,5, '4.7 UF:','LB',0,'L');
    $pdf->Cell(0,5, @$marcaTerceiro['uf'],'BR',1,'L');
  }

  function natureza($pdf) {
    global $solicitacao;
    $natureza = $solicitacao['natureza'];
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '5 - NATUREZA DA SOLICITAÇÃO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '5.1 SOLICITAÇÃO','TLR',1,'L');
    $pdf->Cell(20,5, '1 ('.check($natureza == 'Registro').') Registro','LB',0,'L');
    $pdf->Cell(35,5, '2 ('.check($natureza == 'Renovação do Registro').') Renovação do Registro','B',0,'L');
    $pdf->Cell(50,5, '3 ('.check($solicitacao['natureza'] == 'Alteração de composição do produto').') Alteração de composição do produto','B',0,'L');
    $pdf->Cell(0,5, '4 ('.check($natureza == 'Alteração do processo de fabricação').') Alteração do processo de fabricação','BR',1,'L');
  }

  function identificacaoProduto($pdf) {
    global $produto;
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '6 - IDENTIFICAÇÃO DO PRODUTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(33,5, '6.1 NOME DO PRODUTO:','TL',0,'L');
    $pdf->Cell(0,5, $produto['nome'],'TR',1,'L');

    $pdf->Cell(41,5, '6.2 MARCA / NOME FANTASIA:','TBL',0,'L');
    $pdf->Cell(0,5, $produto['marca'],'BTR',1,'L');
  }

  function rotuloEmbalagem($pdf) {
    global $produto;

    $rotulo = $produto['tipo_rotulo'];
    $primaria = $produto['tipo_primaria'];
    $secundaria = $produto['tipo_secundaria'];

    $primariaOutro = !in_array($primaria, array('Papel', 'Plástica', 'Vidro'));
    $secundariaOutro = !in_array($secundaria, array('Papel/Papelão', 'Plástico', 'Caixa Plástica'));

    $primariaOutroText = $primariaOutro ? '(X) '.$primaria : '( ) ...........................';
    $secundariaOutroText = $secundariaOutro ? '(X) '.$secundaria : '( ) ...........................';

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '7 - CARACTERÍSTICAS DO RÓTULO E DA EMBALAGEM (A embalagem primária deve ser de material aprovado para uso em alimentos).',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '7.1 ROTULO','TLR',1,'L');

    $pdf->Cell(35,5, '1 ('.check($rotulo == '1').') Impresso','LB',0,'L');
    $pdf->Cell(35,5, '2 ('.check($rotulo == '2').') Gravado a quente','B',0,'L');
    $pdf->Cell(35,5, '3('.check($rotulo == '3').') Etiqueta Interna','B',0,'L');
    $pdf->Cell(35,5, '4('.check($rotulo == '4').') Etiqueta Externa','B',0,'L');
    $pdf->Cell(0,5, '5 ('.check($rotulo == '5').') Gravado em relevo','BR',1,'L');

    $pdf->Cell(0,5, '7.2 EMBALAGEM','LR',1,'L');
    $pdf->Cell(35,5, 'PRIMÁRIA:','L',0,'L');
    $pdf->Cell(35,5, '1 ('.check($primaria == 'Papel').') Papel','',0,'L');
    $pdf->Cell(35,5, '2 ('.check($primaria == 'Plástica').') Plástica ','',0,'L');
    $pdf->Cell(35,5, '3 ('.check($primaria == 'Vidro').') Vidro','',0,'L');
    $pdf->Cell(0,5, '4 '.$primariaOutroText,'R',1,'L');

    $pdf->Cell(35,5, 'SECUNDÁRIA','LB',0,'L');
    $pdf->Cell(35,5, '1 ('.check($secundaria == 'Papel/Papelão').') Papel/Papelão ','B',0,'L');
    $pdf->Cell(35,5, '2 ('.check($secundaria == 'Plástico').') Plástico','B',0,'L');
    $pdf->Cell(35,5, '3 ('.check($secundaria == 'Caixa Plástica').') Caixa Plástica','B',0,'L');
    $pdf->Cell(0,5, '4 '.$secundariaOutroText,'BR',1,'L');
  }

  function quantidade($pdf) {
    global $produto;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '8 - QUANTIDADE / FORMA DE INDICAÇÃO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(90,5, '8.1 QTDE DE PRODUTO ACONDICIONADO/UNIDADE DE MEDIDA','TLR',0,'L');
    $pdf->Cell(0,5, '8.2 DATA DE FABRICAÇÃO, DATA DE VALIDADE E LOTE','TR',1,'L');

    $pdf->Cell(90,5, '1- Quantidade acondicionada na embalagem primária: '.$produto['quantidade_primaria'],'L',0,'L');
    $pdf->Cell(0,5, '1- Local: '.$produto['local_fabricacao'],'LR',1,'L');

    $pdf->Cell(90,5, '2- Peso da embalagem: '.$produto['peso_embalagem'],'L',0,'L');
    $pdf->Cell(0,5, '2- Tipo de aplicação: '.$produto['tipo_aplicacao'],'LR',1,'L');

    $pdf->Cell(90,5, '3- Quantidade acondicionada na embalagem secundária: '.$produto['quantidade_secundaria'],'LB',0,'L');
    $pdf->Cell(0,5, '3- Forma de Indicação: '.$produto['forma_indicacao'],'LBR',1,'L');
  }

  function identificacaoLote($pdf) {
    global $produto;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '9 - IDENTIFICAÇÃO DE LOTE',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '9.1 DESCREVER COMO É FEITA A IDENTIFICAÇÃO DO LOTE','TLR',1,'L');

    $pdf->MultiCell(0, 5, $produto['descricao_lote'],'BLR', 'L');
  }

  function composicao($pdf) {
    global $produto, $materiasPrimas;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '10 - COMPOSIÇÃO DO PRODUTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(120,5, '10.1 MATÉRIA PRIMA - INGREDIENTES - ADITIVOS (nº. registro no Órgão competente)','TBLR',0,'L');
    $pdf->Cell(35,5, 'Kg ou L','TBLR',0,'C');
    $pdf->Cell(0,5, '%','TBLR',1,'C');

    foreach ($materiasPrimas as $materiaPrima) {
      $pdf->Cell(120,5, $materiaPrima['ingrediente'],'L',0,'L');
      $pdf->Cell(35,5, $materiaPrima['quantidade']." ".$materiaPrima['medida'],'L',0,'C');
      $pdf->Cell(0,5, $materiaPrima['porcentagem'].'%','LR',1,'C');
    }

    $pdf->SetFont('Times','B',12);
    $pdf->Cell(120,8, '10.2 CONTÉM','TLR',0,'L');
    $pdf->Cell(35,8, 'TOTAL','TR',0,'C');
    $pdf->Cell(0,8, '100,00%','TR',1,'C');

    $pdf->SetFont('Times','',10);

    $pdf->Cell(60,5, 'Glúten','L',0,'L');
    $pdf->Cell(60,5, '( '.check($produto['gluten']).') sim ('.check(!$produto['gluten']).') não','R',0,'L');
    $pdf->Cell(35,5, '','R',0,'C');
    $pdf->Cell(0,5, '','R',1,'C');

    $pdf->Cell(60,5, 'Amarelo tartrazina','L',0,'L');
    $pdf->Cell(60,5, '('.check($produto['tartrazina']).') sim ('.check(!$produto['tartrazina']).') não','R',0,'L');
    $pdf->Cell(35,5, '','R',0,'C');
    $pdf->Cell(0,5, '','R',1,'C');

    $pdf->Cell(60,5, 'Fenilalanina','L',0,'L');
    $pdf->Cell(60,5, '('.check($produto['fenilalanina']).') sim ('.check(!$produto['fenilalanina']).') não','R',0,'L');
    $pdf->Cell(35,5, '','R',0,'C');
    $pdf->Cell(0,5, '','R',1,'C');

    $pdf->Cell(60,5, 'Ingrediente com origem transgênica','L',0,'L');
    $pdf->Cell(60,5, '('.check($produto['transgenicos']).') sim ('.check(!$produto['transgenicos']).') não','R',0,'L');
    $pdf->Cell(35,5, '','R',0,'C');
    $pdf->Cell(0,5, '','R',1,'C');

    $pdf->Cell(60,5, 'Corante','L',0,'L');
    $pdf->Cell(60,5, '('.check($produto['corante']).') sim ('.check(!$produto['corante']).') não','R',0,'L');
    $pdf->Cell(35,5, '','R',0,'C');
    $pdf->Cell(0,5, '','R',1,'C');

    $pdf->Cell(60,5, 'Aromatizante','LB',0,'L');
    $pdf->Cell(60,5, '('.check($produto['aromatizante']).') sim ('.check(!$produto['aromatizante']).') não','RB',0,'L');
    $pdf->Cell(35,5, '','RB',0,'C');
    $pdf->Cell(0,5, '','RB',1,'C');
  }

  function processoFabricacao($pdf) {
    global $produto;
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '11 - PROCESSO DE FABRICAÇÃO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '11.1 DESCRIÇÃO DO PROCESSO E FLUXOGRAMA (EM CAIXILHO) - Incluir desde a recepção até expedição do produto.','TLR',1,'L');

    $pdf->MultiCell(0, 5, $produto['processo_fabricacao'],'BLR', 'L');
  }

  function naturezaProduto($pdf) {
    global $produto;
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '12 - NATUREZA E PROPRIEDADE DO PRODUTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,4, '12.1 Informação referente a natureza, qualidade e propriedade do produto - Atender o disposto no RTIQ ou definir as características do produto sem RTIQ com','TLR',1,'L');
    $pdf->Cell(0,3, 'embasamento científico. Citar o prazo de validade. Envoltório utilizado. Forma do produto.','LR',1,'L');

    $pdf->MultiCell(0, 5, $produto['natureza'],'BLR', 'L');
  }

  function sistemaEmbalagem($pdf) {
    global $produto;
    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '13 - SISTEMA DE EMBALAGEM E ROTULAGEM',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '13.1 DESCRIÇÃO','LTR',1,'L');

    $pdf->MultiCell(0, 5, $produto['sistema_rotulagem'],'BLR', 'L');
  }

  function armazenamento($pdf) {
    global $produto;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '14 - ARMAZENAMENTO DO PRODUTO PRONTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '14.1 DESCRIÇÃO','LTR',1,'L');

    $pdf->Cell(0,5, '1 - Local de Armazenamento: '.$produto['local_armazenamento'],'LR',1,'L');
    $pdf->Cell(0,5, '2 - Forma de Armazenamento: '.$produto['forma_armazenamento'],'LR',1,'L');
    $pdf->Cell(0,5, '3 - Temperatura do Local de Armazenamento: '.$produto['temperatura_armazenamento'].'°C','LR',1,'L');
    $pdf->Cell(0,5, '4 - Temperatura do produto: '.$produto['temperatura_produto'].'°C','LBR',1,'L');
  }

  function transporte($pdf) {
    global $produto;

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '15 - TRANSPORTE DO PRODUTO',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '15.1 DESCRIÇÃO','LTR',1,'L');

    $pdf->Cell(0,5, '1 - Meio de transporte: '.$produto['meio_transporte'],'LR',1,'L');
    $pdf->Cell(0,5, '2 - Condições de transporte: '.$produto['condicao_transporte'],'LR',1,'L');
    $pdf->Cell(0,5, '3 - Forma de acondicionamento: '.$produto['forma_acondicionamento'],'LR',1,'L');
    $pdf->Cell(0,5, '4 - Temperatura de conservação do produto: '.$produto['temperatura_transporte'].'°C','LBR',1,'L');
  }

  function documentos($pdf) {
    global $produto;
    $documentTypes = additionalDocuments();

    $pdf->SetFont('Times','B',8);

    $pdf->Cell(10,5, '16 - DOCUMENTOS ACOMPANHANTES',0,1,'L');

    $pdf->SetFont('Times','',8);
    $pdf->Cell(0,5, '16.1 - RELACIONAR','LTR',1,'L');

    foreach ($documentTypes as $documentType) {
      $havesDocument = produtoAdditionalFile($produto['id'], $documentType->type) != null;
      $pdf->Cell(0,5, '('.check($havesDocument).') '.$documentType->title,'LR',1,'L');
    }

    $pdf->Cell(0,5, '(  ) Entre outros.','LBR',1,'L');
  }

  function check($value) {
    return $value ? "X" : "  ";
  }

  function saveFileToDb($fileName) {
    $conn = OpenCon();

    $tableName = 'arquivos';
    $fields = 'url';
    $values = "'$fileName'";

    $query = InsertQuery($tableName, $fields, $values);
    $queryResult = mysqli_query($conn, $query);

    $fileId = mysqli_insert_id($conn);
    CloseCon($conn);

    return $fileId;
  }

  function generateSolicitacao($id) {
    global $solicitacao, $produto, $materiasPrimas, $marcaTerceiro, $estabelecimento;
    $conn  = OpenCon();

    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'solicitacoes';

    $query = ShowQuery($tableName, $id);
    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $solicitacao = $row;
      }
    }

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $solicitacao['produto_id']);
    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }
    }

    $tableName = 'materias_primas';
    $where = "produto_id = ".$produto['id'];

    $query = ShowQueryWhere($tableName, $where);
    $queryResult = mysqli_query($conn, $query);

    $materiasPrimas = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($materiasPrimas, $row);
    }

    $tableName = 'marca_terceiros';
    $query = ShowQueryWhere($tableName, $where);
    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $marcaTerceiro = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);
    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $estabelecimento = $row;
    }

    $uuid = substr(md5(rand()),0,10);
    $fileName = 'uploads/'.$uuid.$produto['nome'].".pdf";

    $pdf = new PDF();
    $pdf->SetAutoPageBreak(true, 40);
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->SetFont('Times','B',8);
    $pdf->Cell(0,8, 'FORMULÁRIO DE REGISTRO DE PRODUTOS DE ORIGEM ANIMAL',0,1,'C');

    identificacao($pdf);
    peticao($pdf);
    identificacaoEstabelecimento($pdf);
    marcaTerceiros($pdf);
    natureza($pdf);
    identificacaoProduto($pdf);
    rotuloEmbalagem($pdf);
    quantidade($pdf);
    identificacaoLote($pdf);
    composicao($pdf);
    processoFabricacao($pdf);
    naturezaProduto($pdf);
    sistemaEmbalagem($pdf);
    armazenamento($pdf);
    transporte($pdf);
    documentos($pdf);

    $pdf->Output('F', $fileName);

    return saveFileToDb($fileName);
  }
?>
