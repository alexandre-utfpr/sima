<?php
  require_once 'lib/fpdf/fpdf.php';
  require_once 'util/strings.php';
  require_once 'util/db_connection.php';
  require_once 'util/page_utils.php';

  // Cria PDF de requerimento de registro de estabelecimento
  function generateSolicitacao($estabelecimento) {
    $conn  = OpenCon();
    $tableName = 'cadastros';

    $query = ShowQuery($tableName, $estabelecimento['cadastro_id']);

    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $produtor = $row;
    }

    $tableName = 'usuarios';

    $query = ShowQueryWhere($tableName, "cadastro_id = ".$produtor['id']);

    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $usuario = $row;
    }

    CloseCon($conn);

    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $fileName = $estabelecimento['razao_social'].'.pdf';
    $pdf = new FPDF();

    $pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(0,15,'',0,1,'C');
    $pdf->Cell(0,5,'REQUERIMENTO PARA REGISTRO DE ESTABELECIMENTO',0,1,'C');
    $pdf->Cell(0,15,'',0,1,'C');

    $pdf->SetFont('Arial','',12);
    $text = "                                            Eu, ".$produtor['nome']." representante legal, Sócio, proprietário/responsável pela empresa/estabelecimento (razão social) ".$estabelecimento['razao_social']." CNPJ Nº ".$estabelecimento['documento']." situado à rua ".$estabelecimento['endereco']."; nº".$estabelecimento['numero']."; bairro".$estabelecimento['bairro']."; telefone ".$estabelecimento['telefone'].", celular ".$produtor['celular'];
    if (isset($usuario)) {
      $text .= " e-mail ".$usuario['email'];
    }
    $text .= "em Guarapuava-PR; solicito REGISTRO da empresa/estabelecimento ".$estabelecimento['razao_social']." junto ao Serviço de Inspeção Municipal, (SIM/POA) da Secretaria de Agricultura de Guarapuava.\n                                            Declaro que este pedido contém informações exatas, e aceito as normas adotadas pelo Serviço de Inspeção do Município de Guarapuava (SIM/POA), bem como em atender às exigências técnicas e higiênico-sanitárias estabelecidas pelo órgão fiscalizador.";

    $pdf->MultiCell(0, 8, $text, 0, 'L');
    $pdf->Cell(0,40,'',0,1,'C');

    $pdf->Cell(100,15,'',0,0,'R');
    $pdf->Cell(0,15,'Guarapuava, '.date("d").' de '.strftime('%B', strtotime('today')).' de '.date("Y"),0,1,'L');

    $pdf->Cell(130,15,'',0,0,'R');
    $pdf->Cell(0,15,'Atenciosamente,',0,1,'L');

    $pdf->Cell(0,25,'',0,1,'C');

    $pdf->Cell(80,15,'',0,0,'R');
    $pdf->Cell(90,15,'Nome: ________________________________','T',1,'L');
    $pdf->Cell(80,7,'',0,0,'R');
    $pdf->Cell(90,7,'CNPJ: ________________________________',0,0,'L');

    $pdf->Output('I', $fileName);
  }
?>
