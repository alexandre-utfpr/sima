<?php
  session_start();
  require_once 'util/db_connection.php';
  require_once 'util/strings.php';

  // Valida se o POST possui os campos obrigatórios
  function ValidateRequired($post, $requiredFields) {
    foreach ($requiredFields as &$field) {
      if (!isset($post[$field])) {
        return false;
      }
    }

    return true;
  }

  // Valida se a sessão atual pode acessar a página/recurso, de acordo com a restrição $pageRestriction,
  // e com o dono do recurso, caso seja produtor, utilizando $resourceOwner
  function ValidSessionAndResource($pageRestriction, $resourceOwner) {
    if ($_SESSION['type'] == 'admin') {
      return true;
    }

    $validSession = $_SESSION['type'] == $pageRestriction;

    if ($resourceOwner != null) {
      $validOwner = $resourceOwner == $_SESSION['cadastro_id'];
    } else {
      $validOwner = true;
    }

    return ($validSession && $validOwner);
  }

  // Retorna nome do usuário da sessão atual
  function sessionUserName() {
    $conn  = OpenCon();

    $idAtual = $_SESSION["cadastro_id"];

    $tableName = 'cadastros';

    $query = ShowQuery($tableName, $idAtual);

    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $user = $row;
    }

    CloseCon($conn);

    return $user['nome'];
  }

  // Verifica se o produtor atual já fez o cadastro de seu estabelecimento
  function completeRegistration() {
    $conn  = OpenCon();

    $idAtual = $_SESSION["cadastro_id"];

    $tableName = 'estabelecimentos';
    $where = "cadastro_id = $idAtual";

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      return true;
      CloseCon($conn);
    }
    else {
      CloseCon($conn);

      return false;
    }
  }

  // Verifica se o estabelecimento do produtor logado já foi aprovado por um admin
  function approvedBusiness() {
    $conn  = OpenCon();

    $idAtual = $_SESSION["cadastro_id"];

    $tableName = 'estabelecimentos';
    $where = "cadastro_id = $idAtual";

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      return $estabelecimento['aprovado'] == 1;
      CloseCon($conn);
    }
    else {
      CloseCon($conn);
      return false;
    }
  }

  // retorna o id de um estabelecimento dado o id do usuário
  function businessByUserId($id) {
    $conn  = OpenCon();

    $tableName = 'estabelecimentos';
    $where = "cadastro_id = $id";

    $query = ShowQueryWhere($tableName, $where);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      return $estabelecimento['id'];
      CloseCon($conn);
    }
    else {
      CloseCon($conn);
      return null;
    }
  }

  // retorna o id do estabelecimento do produtor logado
  function sessionBusinessId() {
    return businessByUserId($_SESSION["cadastro_id"]);
  }

  // Verifica se o estabelecimento com o id passado já foi aprovado por um admin
  function approvedBusinessById($id) {
    $conn  = OpenCon();

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
    }

    CloseCon($conn);

    return $estabelecimento['aprovado'] == 1;
  }

  // Verifica se um arquivo está dentro dos requisitos passados, whitelist e tamanho máximo
  function checkUpload($file, $whitelist, $size) {
    $target = basename($file["name"]);
    $imageFileType = strtolower(pathinfo($target,PATHINFO_EXTENSION));

    if ($file["size"] > $size) {
      return false;
    }

    if (!in_array($imageFileType, $whitelist)) {
      return false;
    }

    return true;
  }

  // Retorna o caminho de um arquivo dado um id
  function fileUrlFromId($id) {
    $conn  = OpenCon();

    $tableName = 'arquivos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    while($row = $queryResult->fetch_assoc()) {
      $file = $row;
    }

    CloseCon($conn);

    return $file['url'];
  }

  // Sim para 1, Não para 0
  function yesNo($value) {
    return $value == 1 ? "Sim" : "Não";
  }

  // Retorna o tipo de rótulo dado seu número (como um enum)
  function labelType($value) {
    switch ($value) {
      case 1:
        return "Impresso";
        break;
      case 2:
        return "Gravado a quente";
        break;
      case 3:
        return "Etiqueta interna";
        break;
      case 4:
        return "Etiqueta externa";
        break;
      case 5:
        return "Gravado em relevo";
        break;
    }
  }

  // tipos permitidos de documentos adicionais
  function additionalDocumentTypes() {
    return array('responsabilidade_marca',
                  'autorizacao_uso',
                  'registro_marca',
                  'sem_regulamentacao_tecnica',
                  'atendimento_rtiq',
                  'croqui',
                  'fichas_tecnicas',
                  'rotulo',
                  'producao_especfica');
  }

  // tipos permitidos de documentos adicionais, junto com suas descrições
  function additionalDocuments() {
    $documents = array();

    $types = additionalDocumentTypes();

    $titles = array('Declaração de responsabilidade sobre o uso da marca',
                   'Autorização para o uso da marca de terceiro registrado em cartório',
                   'Registro de marca',
                   'Documentos que visam respaldar produtos sem regulamentação técnica',
                   'Declaração de atendimento ao RTIQ e percentual permitido de aditivos no produto final',
                   'Croqui nas cores reais e em escala',
                   'Fichas/Especificações técnicas',
                   'Cópia do rótulo do produto a ser fatiado/fracionado',
                   'Documentos que visam respaldar sistemas de produção específicos (orgânico, caipira), utilização de selos de qualidade, produtos diferenciados, etc.');

    for ($i = 0; $i < count($types); $i++) {
      $document = new stdClass;
      $document->type = $types[$i];
      $document->title = $titles[$i];

      array_push($documents, $document);
    }

    return $documents;
  }

  // retorna o arquivo adicional de um produto, dado seu tipo
  function produtoAdditionalFile($produto, $type) {
    $conn  = OpenCon();

    $tableName = 'arquivos_produtos';
    $where = "tipo = '$type' AND produto_id = '$produto'";

    $query = ShowQueryWhere($tableName, $where);
    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $arquivo = $row;
      }

      return $arquivo;
      CloseCon($conn);
    }
    else {
      CloseCon($conn);
      return null;
    }
  }

  // Formata a data do MYSQL para data pt-BR
  function formatDate($date) {
    $date = DateTime::createFromFormat('Y-m-d', $date);
    return $date->format('d/m/Y');
  }
?>
