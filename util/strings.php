<?php
  //String utilizadas repetidamente pelo sistema
  $invalidFieldsError = 'Favor, verificar os dados inseridos e os campos obrigatórios.';
  $invalidFileError = 'Favor, verificar se o arquivo selecionado está de acordo com os requisitos.';
  $resourceNotFound = 'Recurso não encontrado.';
  $cantDeleteYourself = 'Você não pode deletar o próprio cadastro.';
  $contactSupport = 'Erro inesperado. Entre em contato com o suporte.';
  $invalidLogin = 'Login inválido, verifique os dados inseridos';
  $invalidPassword = 'Senha e confirmação divergentes.';
  $duplicateUser = 'Já existe um usuário cadastrado com esse e-mail.';
  $errorUploading = 'Erro enviando seu arquivo. Entre em contato com o suporte.';
  $userNotFound = 'Usuário não encontrado, favor verificar os dados inseridos';
  $target_dir = "uploads/";
  $ufs = array("PR",
        "AL",
        "AP",
        "AM",
        "BA",
        "CE",
        "DF",
        "ES",
        "GO",
        "MA",
        "MT",
        "MS",
        "MG",
        "PA",
        "PB",
        "AC",
        "PE",
        "PI",
        "RJ",
        "RN",
        "RS",
        "RO",
        "RR",
        "SC",
        "SP",
        "SE",
        "TO");
?>
