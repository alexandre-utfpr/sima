<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['id',
'sim_poa',
'classificacao',
'data_entrada'
];

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
}
elseif (isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'estabelecimentos';

  $query = ShowQuery($tableName, $id);

  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $estabelecimento = $row;
    }
  } else {
    $errorMessage = $resourceNotFound;
  }

  CloseCon($conn);
}
elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && !isset($_GET['id'])) {
  $errorMessage = $resourceNotFound;
}
elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (ValidateRequired($_POST, $requiredFields)) {
    $conn  = OpenCon();

    $id = trim($_POST['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'estabelecimentos';

    $simPoa = trim($_POST['sim_poa']);
    $classificacao = trim($_POST['classificacao']);
    $data_entrada = trim($_POST['data_entrada']);

    $data_entrada = DateTime::createFromFormat('d/m/Y', $data_entrada);

    $simPoa = mysqli_real_escape_string($conn, $simPoa);
    $classificacao = mysqli_real_escape_string($conn, $classificacao);
    $data_entrada = $data_entrada->format('Y-m-d');

    $tableName = 'estabelecimentos';

    $fields = "aprovado = TRUE, ";
    $fields .= "sim_poa = '$simPoa', ";
    $fields .= "data_entrada = '$data_entrada', ";
    $fields .= "classificacao = '$classificacao'";

    mysqli_begin_transaction($conn);

    $query = UpdateQuery($tableName, $fields, $id);
    $queryResult = mysqli_query($conn, $query);

    if ($queryResult) {
      mysqli_commit($conn);
      CloseCon($conn);
      header("Location: adminEstabelecimentoShow.php?id=".$id, true, 301);
    }
    else {
      mysqli_rollback($conn);
      $errorMessage = "$contactSupport";
      CloseCon($conn);
    }
  }
  else {
      $errorMessage = $invalidFieldsError;
    }
  }
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = $estabelecimento['razao_social']; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <?php if(!isset($errorMessage) || ($errorMessage != $resourceNotFound)): ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
               <h6 class="m-0 font-weight-bold text-success">Aprovar estabelecimento | <?php echo $estabelecimento['razao_social']; ?></h6>
             </div>

             <div class="card-body">
               <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="adminEstabelecimentoApprove.php" enctype="multipart/form-data">

                 <input id="id" name="id"  value="<?php echo $estabelecimento['id']; ?>" type="hidden" required>

                 <div class="form-group row">
                   <div class="col-5">
                     <label for="classificacao" class="control-label required-field">Classificação</label>
                     <input id="classificacao" name="classificacao" class="form-control form-control-user"
                     pattern=".{1,40}"
                     placeholder="Classificação" type="text" required>
                   </div>
                 </div>

                 <div class="form-group row">
                   <div class="col-5">
                     <label for="sim_poa" class="control-label required-field">SIM/POA</label>
                     <input id="sim_poa" name="sim_poa" class="form-control form-control-user"
                     placeholder="SIM/POA" type="text" pattern=".{1,40}" required>
                   </div>
                 </div>

                 <div class="form-group row">
                   <div class="col-2">
                    <label for="data_entrada" class="control-label required-field">Data de entrada no SIM/POA</label>
                    <input onkeypress="return false;" type='text' data-language='pt-BR' required name="data_entrada" class="datepicker-here form-control" data-position="right top" />
                   </div>
                 </div>

                 <hr>

                 <a href="javascript:history.go(-1)" class="btn btn-info btn-sm float-right">Voltar</a>
                 <button type="submit" class="btn btn-success btn-sm">Aprovar</button>
               </form>
             </div>
           </div>
         <?php endif; ?>
       </div>
     </div>
   </div>
   <?php include("templates/footer.php");?>
 </div>
</div>
</body>
