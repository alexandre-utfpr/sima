<!DOCTYPE html>
<html lang="pt-br">
<?php include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/landingSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

        <?php include("templates/landingTopbar.php");?>

        <div class="row justify-content-center">

          <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg">
              <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                  <div class="col-lg-6 d-none d-lg-block">
                    <img src="assets/img/bg.jpg" alt="landing">
                  </div>
                  <div class="col-lg-6">
                    <div class="p-5">
                      <div class="text-center">
                        <h1 class="h1 text-success mb-2"><i class="fas fa-fw fa-leaf"></i> SIMA</h1>
                        <h4 class="h4 text-success mb-3">Serviço de Inspeção Municipal Admin</h4>
                        <p class="mb-4">Seja bem-vindo ao <i class="fas fa-fw fa-leaf"></i>  SIMA! Aqui você, produtor rural, cadastra seu estabelecimento, mantém controle de seus produtos e de seus dados, fica de olho nas legislações vigentes, confere documentos pertinentes à produção rural, e agiliza sua conformidade com a prefeitura da cidade.</p>
                      </div>
                      <form class="user">
                        <a href="newCadastro.php" class="btn btn-success btn-user btn-block">
                          Cadastre-se
                        </a>
                      </form>
                      <hr>
                      <div class="text-center">
                        <a class="small text-success" href="legislations.php">Legislações e documentos</a>
                      </div>
                      <div class="text-center">
                        <a class="small text-success" href="login.php?id=2">Já tem uma conta? Entre agora!</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
