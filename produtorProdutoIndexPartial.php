<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();

  $tableName = 'produtos';

  $where = "estabelecimento_id = ".sessionBusinessId();

  $query = IndexQuery($tableName, $where);

  $queryResult = mysqli_query($conn, $query);

  if ($queryResult) {
    $produtos = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($produtos, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }

  CloseCon($conn);
}
?>

<?php if(isset($errorMessage)): ?>
  <?php include("templates/error.php");?>
  <?php else: ?>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Marca / Nome Fantasia</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nome</th>
              <th>Marca / Nome Fantasia</th>
            </tr>
          </tfoot>
          <tbody>
            <?php foreach($produtos as &$produto): ?>
              <tr>
                <td><a href='produtoShow.php?id=<?php echo $produto['id']; ?>'><?php echo $produto['nome']; ?></a></td>
                <td><?php echo $produto['marca']; ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <hr>
      <a href="produtorProdutoCreate.php" class="btn btn-info btn-sm">Cadastrar produto</a>
    </div>
  <?php endif; ?>
