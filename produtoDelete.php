<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$documentosAdicionais = additionalDocuments();

$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $produto = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      $resourceOwner = $estabelecimento['cadastro_id'];
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $conn  = OpenCon();

    $idCadastro = trim($_POST['id']);
    $idCadastro = mysqli_real_escape_string($conn, $idCadastro);

    $tableName = 'produtos';

    $query = DeleteQuery($tableName, $idCadastro);

    $queryResult = mysqli_query($conn, $query);

    CloseCon($conn);

    header("Location: produtoIndex.php", true, 301);
  }
  else {
    $errorMessage = $resourceNotFound;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Deletar produto'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Produto</h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-danger">Deletar: <?php echo $produto['nome'] ?></h6>
                  </div>

                  <div class="card-body">
                    <p>Deseja mesmo deletar o produto <span class="font-weight-bold">"<?php echo $produto['nome'] ?>"</span>? Todos os recursos atrelados à esse produto também serão removidos (marca de terceiros, matérias primas, etc.)</p>
                    <form id="cadastroCreateForm" data-toggle="validator" role="form" method="post" action="produtoDelete.php">
                      <input id="id" name="id" type="hidden" value="<?php echo $produto['id']; ?>">

                      <a href="javascript:history.go(-1)" class="btn btn-info btn-sm">Voltar</a>
                      <button type="submit" class="btn btn-danger btn-sm float-right">Deletar</button>
                    </form>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
