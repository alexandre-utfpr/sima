function toggleFields() {
  var fields = document.getElementById('aprovado_fields');
  var classificacao = document.getElementById('classificacao');
  var sim_poa = document.getElementById('sim_poa');
  var data_entrada = document.getElementById('data_entrada');

  if (fields.style.display === "none") {
    fields.style.display = "block";
    classificacao.setAttribute("required","required");
    sim_poa.setAttribute("required","required");
    data_entrada.setAttribute("required","required");
  } else {
    fields.style.display = "none";
    classificacao.removeAttribute("required");
    sim_poa.removeAttribute("required");
    data_entrada.removeAttribute("required");
    classificacao.value = null;
    sim_poa.value = null;
    data_entrada.value = null;
  }
}
