<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$documentosAdicionais = additionalDocuments();

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'solicitacoes';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $solicitacao = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  else {
    $errorMessage = $resourceNotFound;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Documentos adicionais'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Documentos adicionais</h1>
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success">Documentos adicionais</h6>
                  </div>

                  <div class="card-body">
                    <p>Os documentos aqui presentes referem-se aos documentos anexados ao produto no momento da criação da solicitação, e não são afetados por edições feitas no produto ou seus documentos, bem como a deleção dos mesmos.</p>
                    <h5 class='h5 text-success'>Documentos adicionais </h5>
                    <hr>

                    <p>
                      <span class="font-weight-bold">Declaração de responsabilidade sobre o uso da marca:
                        <?php if($solicitacao['responsabilidade_marca_id'] != null):?>
                          <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['responsabilidade_marca_id']); ?>" target="_blank">
                            Visualizar
                          </a>
                          <?php else: ?>
                            Não adicionado
                          <?php endif; ?>
                        </span>
                      </p>

                      <p>
                        <span class="font-weight-bold">Autorização para o uso da marca de terceiro registrado em cartório:
                          <?php if($solicitacao['autorizacao_uso_id'] != null):?>
                            <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['autorizacao_uso_id']); ?>" target="_blank">
                              Visualizar
                            </a>
                            <?php else: ?>
                              Não adicionado
                            <?php endif; ?>
                          </span>
                        </p>

                        <p>
                          <span class="font-weight-bold">Registro de marca:
                            <?php if($solicitacao['registro_marca_id'] != null):?>
                              <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['registro_marca_id']); ?>" target="_blank">
                                Visualizar
                              </a>
                              <?php else: ?>
                                Não adicionado
                              <?php endif; ?>
                            </span>
                          </p>

                          <p>
                            <span class="font-weight-bold">Documentos que visam respaldar produtos sem regulamentação técnica:
                              <?php if($solicitacao['sem_regulamentacao_tecnica_id'] != null):?>
                                <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['sem_regulamentacao_tecnica_id']); ?>" target="_blank">
                                  Visualizar
                                  <?php else: ?>
                                    Não adicionado
                                  <?php endif; ?>
                                </a>
                              </span>
                            </p>

                            <p>
                              <span class="font-weight-bold">Declaração de atendimento ao RTIQ e percentual permitido de aditivos no produto final:
                                <?php if($solicitacao['atendimento_rtiq_id'] != null):?>
                                  <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['atendimento_rtiq_id']); ?>" target="_blank">
                                    Visualizar
                                  </a>
                                  <?php else: ?>
                                    Não adicionado
                                  <?php endif; ?>
                                </span>
                              </p>

                              <p>
                                <span class="font-weight-bold">Croqui nas cores reais e em escala:
                                  <?php if($solicitacao['croqui_id'] != null):?>
                                    <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['croqui_id']); ?>" target="_blank">
                                      Visualizar
                                    </a>
                                    <?php else: ?>
                                      Não adicionado
                                    <?php endif; ?>
                                  </span>
                                </p>

                                <p>
                                  <span class="font-weight-bold">Fichas/Especificações técnicas:
                                    <?php if($solicitacao['fichas_tecnicas_id'] != null):?>
                                      <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['fichas_tecnicas_id']); ?>" target="_blank">
                                        Visualizar
                                      </a>
                                      <?php else: ?>
                                        Não adicionado
                                      <?php endif; ?>
                                    </span>
                                  </p>

                                  <p>
                                    <span class="font-weight-bold">Cópia do rótulo do produto a ser fatiado/fracionado:
                                      <?php if($solicitacao['rotulo_id'] != null):?>
                                        <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['rotulo_id']); ?>" target="_blank">
                                          Visualizar
                                        </a>
                                        <?php else: ?>
                                          Não adicionado
                                        <?php endif; ?>
                                      </span>
                                    </p>

                                    <p>
                                      <span class="font-weight-bold">Documentos que visam respaldar sistemas de produção específicos (orgânico, caipira), utilização de selos de qualidade, produtos diferenciados, etc.:
                                        <?php if($solicitacao['producao_especfica_id'] != null):?>
                                          <a class='text-info' href="<?php echo fileUrlFromId($solicitacao['producao_especfica_id']); ?>" target="_blank">
                                            Visualizar
                                          </a>
                                          <?php else: ?>
                                            Não adicionado
                                          <?php endif; ?>
                                        </span>
                                      </p>
                                      <hr>
                                      <a href="solicitacaoIndex.php" class="btn btn-info btn-sm">Voltar</a>
                                    </div>
                                  <?php endif; ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php include("templates/footer.php");?>
                        </div>
                      </div>
                    </body>
