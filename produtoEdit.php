<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['nome',
'marca',
'tipo_rotulo',
'tipo_primaria',
'quantidade_primaria',
'peso_embalagem',
'local_fabricacao',
'tipo_aplicacao',
'forma_indicacao',
'descricao_lote',
'processo_fabricacao',
'natureza',
'sistema_rotulagem',
'local_armazenamento',
'forma_armazenamento',
'temperatura_armazenamento',
'temperatura_produto',
'meio_transporte',
'condicao_transporte',
'forma_acondicionamento',
'temperatura_transporte'
];


$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $produto = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      $resourceOwner = $estabelecimento['cadastro_id'];
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $conn  = OpenCon();

      $sisbi = trim($_POST['sisbi']);
      $municipal = trim($_POST['municipal']);
      $nome = trim($_POST['nome']);
      $marca = trim($_POST['marca']);
      $registro = trim($_POST['registro']);
      $tipo_rotulo = trim($_POST['tipo_rotulo']);
      $quantidade_primaria = trim($_POST['quantidade_primaria']);
      $peso_embalagem = trim($_POST['peso_embalagem']);
      $quantidade_secundaria = trim($_POST['quantidade_secundaria']);
      $local_fabricacao = trim($_POST['local_fabricacao']);
      $tipo_aplicacao = trim($_POST['tipo_aplicacao']);
      $forma_indicacao = trim($_POST['forma_indicacao']);
      $descricao_lote = trim($_POST['descricao_lote']);
      $processo_fabricacao = trim($_POST['processo_fabricacao']);
      $natureza = trim($_POST['natureza']);
      $sistema_rotulagem = trim($_POST['sistema_rotulagem']);
      $gluten = trim($_POST['gluten']);
      $tartrazina = trim($_POST['tartrazina']);
      $fenilalanina = trim($_POST['fenilalanina']);
      $transgenicos = trim($_POST['transgenicos']);
      $corante = trim($_POST['corante']);
      $aromatizante = trim($_POST['aromatizante']);
      $tipo_primaria = trim($_POST['tipo_primaria']);
      $tipo_secundaria = trim($_POST['tipo_secundaria']);
      $local_armazenamento = trim($_POST['local_armazenamento']);
      $forma_armazenamento = trim($_POST['forma_armazenamento']);
      $temperatura_armazenamento = trim($_POST['temperatura_armazenamento']);
      $temperatura_produto = trim($_POST['temperatura_produto']);
      $meio_transporte = trim($_POST['meio_transporte']);
      $condicao_transporte = trim($_POST['condicao_transporte']);
      $forma_acondicionamento = trim($_POST['forma_acondicionamento']);
      $temperatura_transporte = trim($_POST['temperatura_transporte']);
      $id = trim($_POST['id']);

      $sisbi = mysqli_real_escape_string($conn, $sisbi);
      $municipal = mysqli_real_escape_string($conn, $municipal);
      $nome = mysqli_real_escape_string($conn, $nome);
      $marca = mysqli_real_escape_string($conn, $marca);
      $registro = mysqli_real_escape_string($conn, $registro);
      $tipo_rotulo = mysqli_real_escape_string($conn, $tipo_rotulo);
      $quantidade_primaria = mysqli_real_escape_string($conn, $quantidade_primaria);
      $peso_embalagem = mysqli_real_escape_string($conn, $peso_embalagem);
      $quantidade_secundaria = mysqli_real_escape_string($conn, $quantidade_secundaria);
      $local_fabricacao = mysqli_real_escape_string($conn, $local_fabricacao);
      $tipo_aplicacao = mysqli_real_escape_string($conn, $tipo_aplicacao);
      $forma_indicacao = mysqli_real_escape_string($conn, $forma_indicacao);
      $descricao_lote = mysqli_real_escape_string($conn, $descricao_lote);
      $processo_fabricacao = mysqli_real_escape_string($conn, $processo_fabricacao);
      $natureza = mysqli_real_escape_string($conn, $natureza);
      $sistema_rotulagem = mysqli_real_escape_string($conn, $sistema_rotulagem);
      $gluten = mysqli_real_escape_string($conn, $gluten);
      $tartrazina = mysqli_real_escape_string($conn, $tartrazina);
      $fenilalanina = mysqli_real_escape_string($conn, $fenilalanina);
      $transgenicos = mysqli_real_escape_string($conn, $transgenicos);
      $corante = mysqli_real_escape_string($conn, $corante);
      $aromatizante = mysqli_real_escape_string($conn, $aromatizante);
      $tipo_primaria = mysqli_real_escape_string($conn, $tipo_primaria);
      $tipo_secundaria = mysqli_real_escape_string($conn, $tipo_secundaria);
      $local_armazenamento = mysqli_real_escape_string($conn, $local_armazenamento);
      $forma_armazenamento = mysqli_real_escape_string($conn, $forma_armazenamento);
      $temperatura_armazenamento = mysqli_real_escape_string($conn, $temperatura_armazenamento);
      $temperatura_produto = mysqli_real_escape_string($conn, $temperatura_produto);
      $meio_transporte = mysqli_real_escape_string($conn, $meio_transporte);
      $condicao_transporte = mysqli_real_escape_string($conn, $condicao_transporte);
      $forma_acondicionamento = mysqli_real_escape_string($conn, $forma_acondicionamento);
      $temperatura_transporte = mysqli_real_escape_string($conn, $temperatura_transporte);
      $id = mysqli_real_escape_string($conn, $id);

      $tableName = 'produtos';

      $fields = "sisbi = '$sisbi', ";
      $fields.= "municipal = '$municipal', ";
      $fields.= "nome = '$nome', ";
      $fields.= "marca = '$marca', ";
      $fields.= "registro = '$registro', ";
      $fields.= "tipo_rotulo = '$tipo_rotulo', ";
      $fields.= "quantidade_primaria = '$quantidade_primaria', ";
      $fields.= "peso_embalagem = '$peso_embalagem', ";
      $fields.= "quantidade_secundaria = '$quantidade_secundaria', ";
      $fields.= "local_fabricacao = '$local_fabricacao', ";
      $fields.= "tipo_aplicacao = '$tipo_aplicacao', ";
      $fields.= "forma_indicacao = '$forma_indicacao', ";
      $fields.= "descricao_lote = '$descricao_lote', ";
      $fields.= "processo_fabricacao = '$processo_fabricacao', ";
      $fields.= "natureza = '$natureza', ";
      $fields.= "sistema_rotulagem = '$sistema_rotulagem', ";
      $fields.= "gluten = '$gluten', ";
      $fields.= "tartrazina = '$tartrazina', ";
      $fields.= "fenilalanina = '$fenilalanina', ";
      $fields.= "transgenicos = '$transgenicos', ";
      $fields.= "corante = '$corante', ";
      $fields.= "aromatizante = '$aromatizante', ";
      $fields.= "tipo_primaria = '$tipo_primaria', ";
      $fields.= "tipo_secundaria = '$tipo_secundaria', ";
      $fields.= "local_armazenamento = '$local_armazenamento', ";
      $fields.= "forma_armazenamento = '$forma_armazenamento', ";
      $fields.= "temperatura_armazenamento = '$temperatura_armazenamento', ";
      $fields.= "temperatura_produto = '$temperatura_produto', ";
      $fields.= "meio_transporte = '$meio_transporte', ";
      $fields.= "condicao_transporte = '$condicao_transporte', ";
      $fields.= "forma_acondicionamento = '$forma_acondicionamento', ";
      $fields.= "temperatura_transporte = '$temperatura_transporte'";

      $query = UpdateQuery($tableName, $fields, $id);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        CloseCon($conn);
        header("Location: produtoShow.php?id=$id", true, 301);
      } else {
        $errorMessage = $contactSupport.$query.mysqli_error($conn);
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Editar produto'; include("templates/header.php");?>
<script src="assets/js/tipos_embalagens.js"></script>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Editar: <?php echo $produto['nome'] ?></h6>
              </div>

              <div class="card-body">
                <form id="cadastroCreateForm" data-toggle="validator" role="form" method="post" action="produtoEdit.php">

                  <h5 class='h5 text-success'>Identificação do produto</h5>
                  <hr>

                  <input name="id" type="hidden" value="<?php echo $produto['id'];?>">
                  <div class="form-group row">
                    <div class="col-5">
                      <label for="nome" class="control-label required-field">Nome do produto</label>
                      <input id="nome" name="nome" class="form-control form-control-user"
                      value="<?php echo $produto['nome'];?>"
                      placeholder="Nome do produto" type="text" pattern=".{1,100}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="marca" class="control-label required-field">Nome / Marca Fantasia</label>
                      <input id="marca" name="marca" class="form-control form-control-user"
                      value="<?php echo $produto['marca'];?>"
                      placeholder="Nome / Marca Fantasia" type="text" pattern=".{1,100}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="registro" class="control-label required-field">Nº de registro do produto</label>
                      <input id="registro" name="registro" class="form-control form-control-user"
                      value="<?php echo $produto['registro'];?>"
                      placeholder="Nº de registro do produto" type="text" pattern=".{1,40}">
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Comercialização</h5>
                  <hr>

                  <input type="hidden" name="sisbi" id="municipal" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="sisbi" id="sisbi" value="1">SISBI
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="municipal" id="municipal" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="municipal" id="municipal" value="1">Municipal
                      </label>
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Rótulo e embalagem</h5>
                  <hr>

                  <div class="form-group row">
                    <div class="col-3">
                      <label for="tipo_rotulo" class="control-label required-field">Tipo rótulo</label>
                      <select name="tipo_rotulo" class="form-control form-control-user">
                        <option value="1">Impresso</option>
                        <option value="2">Gravado a quente</option>
                        <option value="3">Etiqueta interna</option>
                        <option value="4">Etiqueta externa</option>
                        <option value="5">Gravado em relevo</option>
                      </select>
                    </div>
                  </div>

                  <!-- Os valores são definidos por tipos_embalagens.js -->

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="primaria" class="control-label required-field">Tipo da embalagem primária</label>
                      <select id="primaria" class="form-control form-control-user" onchange="setPackageType(this)">
                        <option value="Papel">Papel</option>
                        <option value="Plástica">Plástica</option>
                        <option value="Vidro">Vidro</option>
                        <option value="Outro">Outro</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <input id="tipo_primaria" name="tipo_primaria" class="form-control form-control-user"
                      style="display: none"
                      value="Papel"
                      placeholder="Tipo da embalagem primária" type="text" pattern=".{1,100}" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-4">
                      <label for="secundaria" class="control-label required-field">Tipo da embalagem secundária</label>
                      <select id="secundaria" class="form-control form-control-user" onchange="setPackageType(this)">
                        <option value=""></option>
                        <option value="Papel/Papelão">Papel/Papelão</option>
                        <option value="Plástico">Plástico</option>
                        <option value="Caixa Plástica">Caixa Plástica</option>
                        <option value="Outro">Outro</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <input id="tipo_secundaria" name="tipo_secundaria" class="form-control form-control-user"
                      style="display: none"
                      value="Papel"
                      placeholder="Tipo da embalagem secundária" type="text" pattern=".{1,100}" required>
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Quantidade / Forma de indicação</h5>
                  <hr>

                  <h6 class='h6 text-success'>Quandidade de produto acondicionado/unidade de medida</h6>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="quantidade_primaria" class="control-label required-field">Quantidade acondicionada na embalagem primária</label>
                      <input id="quantidade_primaria" name="quantidade_primaria" class="form-control form-control-user"
                      value="<?php echo $produto['quantidade_primaria'];?>"
                      placeholder="Quantidade acondicionada na embalagem primária" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="quantidade_secundaria" class="control-label required-field">Quantidade acondicionada na embalagem secundária</label>
                      <input id="quantidade_secundaria" name="quantidade_secundaria" class="form-control form-control-user"
                      value="<?php echo $produto['peso_embalagem'];?>"
                      placeholder="Quantidade acondicionada na embalagem secundária" type="text" pattern=".{1,40}">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="peso_embalagem" class="control-label required-field">Peso da embalagem</label>
                      <input id="peso_embalagem" name="peso_embalagem" class="form-control form-control-user"
                      value="<?php echo $produto['peso_embalagem'];?>"
                      placeholder="Peso da embalagem" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <br>
                  <h6 class='h6 text-success'>Data de fabricação, data de validade e lote</h6>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="local_fabricacao" class="control-label required-field">Local</label>
                      <input id="local_fabricacao" name="local_fabricacao" class="form-control form-control-user"
                      value="<?php echo $produto['local_fabricacao'];?>"
                      placeholder="Local" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="tipo_aplicacao" class="control-label required-field">Tipo de aplicação</label>
                      <input id="tipo_aplicacao" name="tipo_aplicacao" class="form-control form-control-user"
                      value="<?php echo $produto['tipo_aplicacao'];?>"
                      placeholder="Tipo de aplicação" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="forma_indicacao" class="control-label required-field">Forma de indicação</label>
                      <input id="forma_indicacao" name="forma_indicacao" class="form-control form-control-user"
                      value="<?php echo $produto['forma_indicacao'];?>"
                      placeholder="Forma de indicação" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Identificação de lote</h5>
                  <hr>

                  <div class="form-group">
                    <label for="descricao_lote" class="control-label required-field">
                      Descreva como é feita a identificação do lote
                    </label>
                    <textarea id="descricao_lote" name="descricao_lote" class="form-control form-control-user" style="resize:none" name="name" rows="8" required><?php echo $produto['descricao_lote'];?></textarea>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Conteúdos</h5>
                  <hr>

                  <input type="hidden" name="gluten" id="gluten" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="gluten" id="gluten" value="1">Glúten
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="tartrazina" id="tartrazina" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="tartrazina" id="tartrazina" value="1">Amarelo tartrazina
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="fenilalanina" id="fenilalanina" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="fenilalanina" id="fenilalanina" value="1">Fenilananina
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="transgenicos" id="transgenicos" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="transgenicos" id="transgenicos" value="1">Ingredientes com origem transgênica
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="corante" id="corante" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="corante" id="corante" value="1">Corante
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="aromatizante" id="aromatizante" value="0">
                  <div class="form-group row">
                    <div class="col-5">
                      <label class="control-label required-field">
                        <input type="checkbox" name="aromatizante" id="aromatizante" value="1">Aromatizante
                      </label>
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Processo de fabricação</h5>
                  <hr>

                  <h6 class='h6 text-success'>Descrição do processo de fabricação</h6>

                  <div class="form-group">
                    <label for="processo_fabricacao" class="control-label required-field">
                      Incluir desde a recepção até expedição do produto
                    </label>
                    <textarea id="processo_fabricacao" name="processo_fabricacao" class="form-control form-control-user" style="resize:none" name="name" rows="8" required><?php echo $produto['processo_fabricacao'];?></textarea>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Natureza e propriedade do produto</h5>
                  <hr>

                  <h6 class='h6 text-success'>Informação referente a natureza, qualidade e propriedade do produto</h6>

                  <div class="form-group">
                    <label for="natureza" class="control-label required-field">Atender o disposto no RTIQ ou definir as características do produto sem RTIQ
                      com embasamento científico. Citar o prazo de validade. Envoltório utilizado. Forma do produto.
                    </label>
                    <textarea id="natureza" name="natureza" class="form-control form-control-user" style="resize:none" name="name" rows="8" required><?php echo $produto['natureza'];?></textarea>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Sistema de embalagem e rotulagem</h5>
                  <hr>

                  <div class="form-group">
                    <label for="sistema_rotulagem" class="control-label required-field">Descrição
                    </label>
                    <textarea id="sistema_rotulagem" name="sistema_rotulagem" class="form-control form-control-user" style="resize:none" name="name" rows="8"required><?php echo $produto['sistema_rotulagem'];?></textarea>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Armazenamento do produto pronto</h5>
                  <hr>

                  <h6 class='h6 text-success'>Descrição</h6>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="local_armazenamento" class="control-label required-field">Local de armazenamento</label>
                      <input id="local_armazenamento" name="local_armazenamento" class="form-control form-control-user"
                      value="<?php echo $produto['local_armazenamento'];?>"
                      placeholder="Local de armazenamento" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="forma_armazenamento" class="control-label required-field">Forma de armazenamento</label>
                      <input id="forma_armazenamento" name="forma_armazenamento" class="form-control form-control-user"
                      value="<?php echo $produto['forma_armazenamento'];?>"
                      placeholder="Forma de armazenamento" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="temperatura_armazenamento" class="control-label required-field">Temperatuda do local de armazenamento (em ºC)</label>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-2">
                      <input id="temperatura_armazenamento" name="temperatura_armazenamento" class="form-control form-control-user" type="number" min="-200" max="200"
                      value="<?php echo $produto['temperatura_armazenamento'];?>"required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="temperatura_produto" class="control-label required-field">Temperatuda do produto (em ºC)</label>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-2">
                      <input id="temperatura_produto" name="temperatura_produto" class="form-control form-control-user" type="number" min="-200" max="200"
                      value="<?php echo $produto['temperatura_produto'];?>"required>
                    </div>
                  </div>

                  <br>
                  <h5 class='h5 text-success'>Transporte do produto pronto</h5>
                  <hr>

                  <h6 class='h6 text-success'>Descrição</h6>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="meio_transporte" class="control-label required-field">Meio de transporte</label>
                      <input id="meio_transporte" name="meio_transporte" class="form-control form-control-user"
                      value="<?php echo $produto['meio_transporte'];?>"
                      placeholder="Meio de transporte" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="condicao_transporte" class="control-label required-field">Condições de transporte</label>
                      <input id="condicao_transporte" name="condicao_transporte" class="form-control form-control-user"
                      value="<?php echo $produto['condicao_transporte'];?>"
                      placeholder="Condições de transporte" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="forma_acondicionamento" class="control-label required-field">Forma de acondicionamento</label>
                      <input id="forma_acondicionamento" name="forma_acondicionamento" class="form-control form-control-user"
                      value="<?php echo $produto['forma_acondicionamento'];?>"
                      placeholder="Forma de acondicionamento" type="text" pattern=".{1,40}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-6">
                      <label for="temperatura_transporte" class="control-label required-field">Temperatura de conservação do produto (em ºC)</label>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-2">
                      <input id="temperatura_transporte" name="temperatura_transporte" class="form-control form-control-user" type="number" min="-200" max="200"
                      value="<?php echo $produto['temperatura_transporte'];?>"required>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
