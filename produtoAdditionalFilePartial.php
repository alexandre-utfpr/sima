<?php
$file = produtoAdditionalFile($produto['id'], $documentoAdicional->type);
?>

<?php if($file != null): ?>
  <p>
    <span class="font-weight-bold"><?php echo $documentoAdicional->title; ?>:
      <a class='text-info' href="<?php echo fileUrlFromId($file['arquivo_id']); ?>" target="_blank">
        Visualizar
      </a>

      <span class="font-weight-bold"> | </span>

      <a class='text-success' href="additionalFileEdit.php?id=<?php echo $file['id']; ?>" target="_blank">
        Editar
      </a>

      <span class="font-weight-bold"> | </span>

      <a class='text-danger' href="additionalFileDelete.php?id=<?php echo $file['id']; ?>">
        Deletar
      </a>
    </span>
  </p>
<?php else: ?>
  <p>
    <span class="font-weight-bold"><?php echo $documentoAdicional->title; ?>:
      <a class='text-success' href="additionalFileCreate.php?id=<?php echo $produto['id']; ?>&tipo=<?php echo $documentoAdicional->type; ?>">
        Adicionar
      </a>
    </span>
  </p>
<?php endif; ?>
