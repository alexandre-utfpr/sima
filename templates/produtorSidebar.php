<?php
  require_once 'util/page_utils.php';
?>

<ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="produtorHome.php">
    <div class="sidebar-brand-icon">
      <i class="fas fa-leaf"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SIMA</div>
  </a>

  <hr class="sidebar-divider my-0">

  <li class="nav-item">
    <a class="nav-link" href="produtorHome.php">
      <i class="fas fa-fw fa-leaf"></i>
      <span>Início</span>
    </a>
  </li>

  <hr class="sidebar-divider">

  <li class="nav-item">
    <?php if(completeRegistration()): ?>
      <a class="nav-link" href="produtorEstabelecimentoShow.php">
    <?php else: ?>
      <a class="nav-link" href="produtorEstabelecimentoCreate.php">
    <?php endif; ?>
      <i class="fas fa-fw fa-store"></i>
      <span>Estabelecimento</span></a>
  </li>

  <?php if(approvedBusiness()): ?>
    <li class="nav-item">
      <a class="nav-link" href="produtoIndex.php">
        <i class="fas fa-fw fa-cheese"></i>
        <span>Produtos</span>
      </a>
    </li>
  <?php endif; ?>

  <hr class="sidebar-divider">

  <div class="sidebar-heading">
    Utilidades
  </div>

  <li class="nav-item">
    <a class="nav-link" href="legislations.php">
      <i class="fas fa-fw fa-scroll"></i>
      <span>Legislações</span>
    </a>
  </li>
</ul>
