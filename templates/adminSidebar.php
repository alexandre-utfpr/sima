<ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="adminHome.php">
    <div class="sidebar-brand-icon">
      <i class="fas fa-leaf"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SIMA</div>
  </a>

  <hr class="sidebar-divider my-0">

  <li class="nav-item">
    <a class="nav-link" href="adminHome.php">
      <i class="fas fa-fw fa-leaf"></i>
      <span>Início</span></a>
  </li>

  <hr class="sidebar-divider">

  <div class="sidebar-heading">
    Produtores
  </div>

  <li class="nav-item">
    <a class="nav-link" href="adminCadastroIndex.php">
      <i class="fas fa-fw fa-seedling"></i>
      <span>Produtores</span>
    </a>
  </li>


  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-fw fa-store"></i>
      <span>Estabelecimentos</span></a>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="adminApprovedEstabelecimentoIndex.php">Aprovados</a>
        <a class="collapse-item" href="adminAwaitingEstabelecimentoIndex.php">Aguardando aprovação</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="produtoIndex.php">
      <i class="fas fa-fw fa-cheese"></i>
      <span>Produtos</span></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="solicitacaoIndex.php">
      <i class="fas fa-fw fa-file-alt"></i>
      <span>Solicitações</span>
    </a>
  </li>

  <hr class="sidebar-divider">

  <div class="sidebar-heading">
    Administrador
  </div>

  <li class="nav-item">
    <a class="nav-link" href="adminLegislationIndex.php">
      <i class="fas fa-fw fa-scroll"></i>
      <span>Legislações</span>
    </a>
  </li>
</ul>
