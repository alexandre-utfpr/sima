<?php
  if(isset($title)) {
    $title .= " | SIMA - Serviço de Inspeção Municipal Admin";
  } else {
    $title = "Serviço de Inspeção Municipal Admin";
  }
?>

<head>
  <meta charset="ISO-8859-1">
  <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="SIMA - Serviço de Inspeção Municipal Admin">
  <meta name="author" content="Alexandre Manikowski">

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="assets/css/application.css" rel="stylesheet">

  <link rel="icon" href="assets/img/favicon.ico">

  <script src="vendor/jquery/jquery.min.js" charset="ISO-8859-1"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js" charset="ISO-8859-1"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js" charset="ISO-8859-1"></script>
  <script src="vendor/datatables/jquery.dataTables.js" charset="ISO-8859-1"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js" charset="ISO-8859-1"></script>
  <script src="assets/js/demo/datatables-demo.js"></script>

  <link href="assets/css/datepicker.min.css" rel="stylesheet" type="text/css">
  <script src="assets/js/datepicker.min.js"></script>
  <script src="assets/js/i18n/datepicker.pt-BR.js"></script>

  <title><?php echo $title;?></title>
</head>
