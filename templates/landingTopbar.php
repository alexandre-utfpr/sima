<nav class="navbar navbar-expand navbar-light bg-dark topbar mb-4 static-top shadow">
  <div class="float-left">
    <a class="nav-link text-success" href="index.php">
      <i class="fas fa-fw fa-leaf"></i>
      <span>SIMA</span>
    </a>
  </div>

  <ul class="navbar-nav ml-auto">
    <li class="nav-item d-sm-none">
      <li class="nav-item">
        <a class="nav-link" href="legislations.php">
          <span> <i class="fas fa-fw fa-scroll"></i> Legislações e Documentos</span></a>
      </li>
    </li>

    <div class="topbar-divider d-none d-sm-block"></div>

    <?php if(isset($_SESSION['type'])): ?>
      <li class="nav-item d-sm-none">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo $_SESSION['type'] ;?>Home.php">
            <span>Painel</span>
          </a>
        </li>
      </li>
    <?php else: ?>
      <li class="nav-item d-sm-none">
        <li class="nav-item">
          <a class="nav-link" href="login.php">
            <span>Login</span>
          </a>
        </li>
      </li>
    <?php endif;?>
  </ul>
</nav>
