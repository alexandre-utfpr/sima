<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';
require_once 'util/estabelecimento_pdf_creator.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if (isset($_GET['id']))
  {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      generateSolicitacao($estabelecimento);
    } else {
      CloseCon($conn);
      header("Location: adminApprovedEstabelecimentoIndex.php", true, 301);
    }
  } else {
    header("Location: adminApprovedEstabelecimentoIndex.php", true, 301);
  }
}
?>
