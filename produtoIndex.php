<?php
  require_once 'util/page_utils.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Produtos'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
              <h1 class="h3 mb-4 text-gray-800">Produtos</h1>

              <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-success">Produtos</h6>
                    </div>
                <?php include($_SESSION['type']."ProdutoIndexPartial.php"); ?>
              </div>
            </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
