<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'estabelecimentos';
    $fields = 'id';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $conn  = OpenCon();

    $id = trim($_POST['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'estabelecimentos';

    $query = DeleteQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    CloseCon($conn);

    header("Location: adminApprovedEstabelecimentoIndex.php", true, 301);
  }
  else {
    $errorMessage = $resourceNotFound;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Produtores'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800"><?php echo $title ?></h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-danger">Deletar <?php echo $estabelecimento['razao_social'] ?></h6>
                  </div>

                  <div class="card-body">
                    <p>Deseja mesmo deletar o estabelecimento <span class="font-weight-bold"></span><?php echo $estabelecimento['razao_social'] ?>? Todos os recursos atrelados à esse produtor também serão removidos (produtos, etc.)</p>
                    <form id="cadastroCreateForm" data-toggle="validator" role="form" method="post" action="adminEstabelecimentoDelete.php">
                      <input id="id" name="id" type="hidden" value="<?php echo $estabelecimento['id']; ?>">
                      <a href="javascript:history.go(-1)" class="btn btn-info btn-sm">Voltar</a>
                      <button type="submit" class="btn btn-danger btn-sm float-right">Deletar</button>
                    </form>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </body>
  </html>
