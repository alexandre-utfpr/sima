<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';
$requiredFields = ['id'];

$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'arquivos_produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $arquivo = $row;
    }

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $arquivo['produto_id']);
    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }

      $tableName = 'estabelecimentos';

      $query = ShowQuery($tableName, $produto['estabelecimento_id']);
      $queryResult = mysqli_query($conn, $query);

      if (mysqli_num_rows($queryResult) > 0) {
        while($row = $queryResult->fetch_assoc()) {
          $estabelecimento = $row;
        }

        $resourceOwner = $estabelecimento['cadastro_id'];
      }
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields) && ($_FILES['document']['size'] != 0)) {

      if (checkUpload($_FILES["document"], array('pdf', 'zip'), 5000000)) {
        $uuid = substr(md5(rand()),0,10);
        $target_file = $target_dir . $uuid . '-' . basename($_FILES["document"]["name"]);

        if (move_uploaded_file($_FILES["document"]["tmp_name"], $target_file)) {
          $conn  = OpenCon();

          $id = trim($_POST['id']);
          $id = mysqli_real_escape_string($conn, $id);

          $tableName = 'arquivos';
          $fields = 'url';
          $values = "'$target_file'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          $fileId = mysqli_insert_id($conn);

          $tableName = 'arquivos_produtos';

          $query = ShowQuery($tableName, $id);
          $queryResult = mysqli_query($conn, $query);

          while($row = $queryResult->fetch_assoc()) {
            $arquivo = $row;
          }

          $idProduto = $arquivo['produto_id'];
          $tipoArquivo = $arquivo['tipo'];

          $fields = "produto_id = NULL";

          $query = UpdateQuery($tableName, $fields, $id);
          $queryResult = mysqli_query($conn, $query);

          $tableName = 'arquivos_produtos';
          $fields = 'tipo, produto_id, arquivo_id';
          $values = "'$tipoArquivo', '$idProduto', $fileId";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          if ($queryResult) {
            CloseCon($conn);
            header("Location: produtoShow.php?id=".$idProduto, true, 301);
          } else {
            $errorMessage = $contactSupport.mysqli_error($conn).$query;
            CloseCon($conn);
          }
        } else {
          $errorMessage = $errorUploading.mysqli_error($conn).$query;
        }
      } else {
        $errorMessage = $invalidFileError;
        header("Location: additionalFileEdit.php?id=".$_POST['id'], true, 301);
        return;
      }
    }
    else {
      $errorMessage = $invalidFileError;
      header("Location: additionalFileEdit.php?id=".$_POST['id'], true, 301);
      return;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Incluir documento adicional'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Incluir documento adicional</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="additionalFileEdit.php" enctype="multipart/form-data">
                  <input name="id" type="hidden" value="<?php echo $_GET['id'] ?>">

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="document" class="control-label required-field">Arquivo (PDF ou ZIP, máx. 5Mb)</label>
                      <input type="file" name="document" id="document">
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Editar</button>

                  <a href="additionalFileDelete.php?id=<?php echo $_GET['id']; ?>" class="btn btn-danger btn-sm float-right">
                    <span class="text">Deletar</span>
                  </a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
