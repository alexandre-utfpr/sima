<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
}
else {
  $conn  = OpenCon();

  $tableName = 'legislacoes';

  $query = IndexQuery($tableName, null);

  $queryResult = mysqli_query($conn, $query);

  if ($queryResult) {
    $legislacoes = array();
    while($row = $queryResult->fetch_assoc()) {
      array_push($legislacoes, $row);
    }
  }
  else {
    $errorMessage = $contactSupport;
  }

  CloseCon($conn);
}
?>


<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Legislações e documentos'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include("templates/adminTopbar.php");?>

      <div class="container-fluid">
        <div class="col-12">
          <h1 class="h3 mb-4 text-gray-800">Legislações e documentos</h1>

        <div class="card shadow mb-4">
          <?php if(isset($errorMessage)): ?>
            <?php include("templates/error.php");?>
          <?php else: ?>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Título</th>
                      <th>Visualizar</th>
                      <th>Editar</th>
                      <th>Deletar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Título</th>
                      <th>Visualizar</th>
                      <th>Editar</th>
                      <th>Deletar</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php foreach($legislacoes as &$legislacao): ?>
                      <tr>
                      	<td><?php echo $legislacao['titulo']; ?></td>
                        <td><a href="<?php echo fileUrlFromId($legislacao['arquivo_id']); ?>" target="_blank">Visualizar</a></td>
                        <td><a href="adminLegislationEdit.php?id=<?php echo $legislacao['id']; ?>">Editar</td>
                          <td><a href="adminLegislationDelete.php?id=<?php echo $legislacao['id']; ?>" class="text-danger">Deletar</td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                  <hr>
                  <a href="adminLegislationCreate.php" class="btn btn-info btn-sm">Adicionar legislação</a>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
