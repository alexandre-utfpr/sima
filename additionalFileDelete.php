<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'arquivos_produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $arquivo = $row;
    }

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $arquivo['produto_id']);
    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }

      $tableName = 'estabelecimentos';

      $query = ShowQuery($tableName, $produto['estabelecimento_id']);
      $queryResult = mysqli_query($conn, $query);

      if (mysqli_num_rows($queryResult) > 0) {
        while($row = $queryResult->fetch_assoc()) {
          $estabelecimento = $row;
        }

        $resourceOwner = $estabelecimento['cadastro_id'];
      }
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
} else {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'arquivos_produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  while($row = $queryResult->fetch_assoc()) {
    $arquivo = $row;
  }

  $idProduto = $arquivo['produto_id'];

  $fields = "produto_id = NULL";

  $query = UpdateQuery($tableName, $fields, $id);
  $queryResult = mysqli_query($conn, $query);

  CloseCon($conn);

  header("Location: produtoShow.php?id=".$idProduto, true, 301);
}
?>
