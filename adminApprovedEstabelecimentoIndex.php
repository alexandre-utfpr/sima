<?php
require_once 'util/page_utils.php';
require_once 'util/strings.php';
require_once 'util/db_connection.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Estabelecimentos aprovados'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Estabelecimentos</h1>

            <div class="card shadow mb-4">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Estabelecimentos aprovados</h6>
              </div>
              <div class="card-body">
                <?php include("adminApprovedEstabelecimentoPartial.php");?>
              </div>
            </div>
          </div>
        </div>
        <?php include("templates/footer.php");?>
      </div>
    </div>
  </div>
</body>
