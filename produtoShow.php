<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$documentosAdicionais = additionalDocuments();

$pageRestriction = 'produtor';
$resourceOwner = null;

if(isset($_GET['id'])) {
  $conn  = OpenCon();

  $id = trim($_GET['id']);
  $id = mysqli_real_escape_string($conn, $id);

  $tableName = 'produtos';

  $query = ShowQuery($tableName, $id);
  $queryResult = mysqli_query($conn, $query);

  if (mysqli_num_rows($queryResult) > 0) {
    while($row = $queryResult->fetch_assoc()) {
      $produto = $row;
    }

    $tableName = 'estabelecimentos';

    $query = ShowQuery($tableName, $produto['estabelecimento_id']);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $estabelecimento = $row;
      }

      $resourceOwner = $estabelecimento['cadastro_id'];
    }
  }

  CloseCon($conn);
}

if (!ValidSessionAndResource($pageRestriction, $resourceOwner)) {
  header("Location: logout.php", true, 301);
} else {
  if (isset($_GET['id'])) {
    $conn  = OpenCon();

    $id = trim($_GET['id']);
    $id = mysqli_real_escape_string($conn, $id);

    $tableName = 'produtos';

    $query = ShowQuery($tableName, $id);

    $queryResult = mysqli_query($conn, $query);

    if (mysqli_num_rows($queryResult) > 0) {
      while($row = $queryResult->fetch_assoc()) {
        $produto = $row;
      }

      $tableName = 'materias_primas';
      $where = "produto_id = ".$produto['id'];

      $query = ShowQueryWhere($tableName, $where);
      $queryResult = mysqli_query($conn, $query);

      $materias_primas = array();
      while($row = $queryResult->fetch_assoc()) {
        array_push($materias_primas, $row);
      }

      $tableName = 'marca_terceiros';
      $query = ShowQueryWhere($tableName, $where);
      $queryResult = mysqli_query($conn, $query);

      while($row = $queryResult->fetch_assoc()) {
        $marcaTerceiro = $row;
      }
    }
    else {
      $errorMessage = $resourceNotFound;
    }

    CloseCon($conn);
  }
  else {
    $errorMessage = $resourceNotFound;
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Produto'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <h1 class="h3 mb-4 text-gray-800">Produto</h1>

            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
                <?php else: ?>
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-success"><?php echo $produto['nome'] ?></h6>
                  </div>

                  <div class="card-body">
                    <h5 class='h5 text-success'>Identificação do produto</h5>
                    <hr>
                    <p><span class="font-weight-bold">Nome: </span><?php echo $produto['nome'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Nome / Marca Fantasia: </span><?php echo $produto['nome'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Nº de registro do produto: </span><?php echo $produto['registro'] ?></p>
                    <?php if ($_SESSION['type'] == 'admin'): ?>
                      <hr>
                      <a href="produtoIndex.php?id=<?php echo $produto['estabelecimento_id'] ?>"><span class="font-weight-bold text-info">Visualizar estabelecimento</span></a>
                      <br>
                    <?php endif;?>
                    <br>

                    <h5 class='h5 text-success'>Comercialização</h5>
                    <hr>
                    <p><span class="font-weight-bold">SISBI: </span><?php echo yesNo($produto['sisbi']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Municipal: </span><?php echo yesNo($produto['municipal']) ?></p>
                    <br>

                    <h5 class='h5 text-success'>Rótulo e embalagem</h5>
                    <hr>
                    <p><span class="font-weight-bold">Tipo do rótulo: </span><?php echo labelType($produto['tipo_rotulo']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Tipo da embalagem primária: </span><?php echo $produto['tipo_primaria'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Tipo da embalagem secundária: </span><?php echo $produto['tipo_secundaria'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Quantidade / Forma de indicação</h5>
                    <hr>
                    <h6 class='h6 text-success'>Quandidade de produto acondicionado/unidade de medida</h6>
                    <br>
                    <p><span class="font-weight-bold">Quantidade acondicionada na embalagem primária: </span><?php echo $produto['quantidade_primaria'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Quantidade acondicionada na embalagem secundária: </span><?php echo $produto['quantidade_secundaria'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Peso da embalagem: </span><?php echo $produto['peso_embalagem'] ?></p>
                    <br>

                    <h6 class='h6 text-success'>Data de fabricação, data de validade e lote</h6>
                    <br>
                    <p><span class="font-weight-bold">Local: </span><?php echo $produto['local_fabricacao'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Tipo de aplicação: </span><?php echo $produto['tipo_aplicacao'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Forma de indicação: </span><?php echo $produto['forma_indicacao'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Identificação de lote</h5>
                    <hr>
                    <p><span class="font-weight-bold">Descrição de como é feita a identificação do lote: </span></p>
                    <p><?php echo $produto['descricao_lote'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Conteúdos</h5>
                    <hr>
                    <p><span class="font-weight-bold">Glúten: </span><?php echo yesNo($produto['gluten']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Amarelo tartrazina: </span><?php echo yesNo($produto['tartrazina']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Fenilalanina: </span><?php echo yesNo($produto['fenilalanina']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Ingredientes com origem transgênica: </span><?php echo yesNo($produto['transgenicos']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Corante: </span><?php echo yesNo($produto['corante']) ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Aromatizante: </span><?php echo yesNo($produto['aromatizante']) ?></p>
                    <br>

                    <h5 class='h5 text-success'>Processo de fabricação</h5>
                    <hr>
                    <p><span class="font-weight-bold">Descrição do processo de fabricação: </span></p>
                    <p><?php echo $produto['processo_fabricacao'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Natureza e propriedade do produto</h5>
                    <hr>
                    <p><span class="font-weight-bold">Atendimento  do disposto no RTIQ, definições das características do produto sem RTIQ
                    com embasamento científico. Prazo de validade. Envoltório utilizado. Forma do produto.: </span></p>
                    <p><?php echo $produto['natureza'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Sistema de embalagem e rotulagem</h5>
                    <hr>
                    <p><span class="font-weight-bold">Descrição: </span></p>
                    <p><?php echo $produto['sistema_rotulagem'] ?></p>
                    <br>

                    <h5 class='h5 text-success'>Armazenamento do produto pronto</h5>
                    <hr>
                    <p><span class="font-weight-bold">Local de armazenamento: </span><?php echo $produto['local_armazenamento'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Forma de armazenamento: </span><?php echo $produto['forma_armazenamento'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Temperatuda do local de armazenamento: </span><?php echo $produto['temperatura_armazenamento'] ?>ºC</p>
                    <br>

                    <h5 class='h5 text-success'>Transporte do produto pronto</h5>
                    <hr>
                    <p><span class="font-weight-bold">Meio de transporte: </span><?php echo $produto['meio_transporte'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Condições de transporte: </span><?php echo $produto['condicao_transporte'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Forma de acondicionamento: </span><?php echo $produto['forma_acondicionamento'] ?></p>
                    <hr>
                    <p><span class="font-weight-bold">Temperatura de conservação do produto: </span><?php echo $produto['temperatura_transporte'] ?>ºC</p>
                    <br>

                    <h5 class='h5 text-success'>Identificação de marca de terceiros: </h5>
                    <hr>

                    <?php if(!isset($marcaTerceiro)): ?>
                      <p><a href='marcaTerceiroCreate.php?id=<?php echo $produto['id']; ?>'><i class="fas fa-fw fa-plus-square"></i> Cadastrar</a></p>
                      <p><span class="font-weight-bold">Nenhuma marca de terceiro cadastrada.</span></p>
                      <?php else: ?>
                        <p>
                          <span class="font-weight-bold">
                            <a class='text-success' href="marcaTerceiroEdit.php?id=<?php echo $marcaTerceiro['id']; ?>" target="_blank">
                              Editar
                            </a>

                            <span class="font-weight-bold"> | </span>

                            <a class='text-danger' href="marcaTerceiroDelete.php?id=<?php echo $marcaTerceiro['id']; ?>">
                              Deletar
                            </a>
                          </span>
                        </p>
                        <p><span class="font-weight-bold">Nome/Razão social: </span><?php echo $marcaTerceiro['nome'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">CNPJ/CPF: </span><?php echo $marcaTerceiro['documento'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">Endereço: </span><?php echo $marcaTerceiro['endereco'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">Número: </span><?php echo $marcaTerceiro['numero'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">Bairro: </span><?php echo $marcaTerceiro['bairro'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">Municipio: </span><?php echo $marcaTerceiro['municipio'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">UF: </span><?php echo $marcaTerceiro['uf'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">CEP: </span><?php echo $marcaTerceiro['cep'] ?></p>
                        <hr>
                        <p><span class="font-weight-bold">Telefone: </span><?php echo $marcaTerceiro['telefone'] ?></p>
                        <hr>
                      <?php endif; ?>

                      <br>
                      <h5 class='h5 text-success'>Matérias-primas: <a href="materiaPrimaCreate.php?id=<?php echo $produto['id']?>"><i class="fas fa-fw fa-plus-square"></i></a></h5>
                      <hr>

                      <?php if(empty($materias_primas)): ?>
                        <p><span class="font-weight-bold">Nenhuma matéria-prima cadastrada.</span></p>
                      <?php endif; ?>

                      <?php foreach($materias_primas as &$materia_prima): ?>
                        <p><span class="font-weight-bold">Ingrediente: </span>
                          <a href='materiaPrimaEdit.php?id=<?php echo $materia_prima['id']; ?>'><?php echo $materia_prima['ingrediente']; ?></a>
                        </p>
                        <p><span class="font-weight-bold">Quantidade: </span><?php echo $materia_prima['quantidade']." ".$materia_prima['medida']." (".$materia_prima['porcentagem']."%)"; ?></p>
                        <p><span class="font-weight-bold"><a class=' text-danger' href="materiaPrimaDelete.php?id=<?php echo $materia_prima['id']; ?>">Remover</a></span></p>
                        <hr>
                      <?php endforeach; ?>

                      <br>
                      <h5 class='h5 text-success'>Documentos adicionais: </h5>
                      <hr>

                      <?php foreach($documentosAdicionais as &$documentoAdicional): ?>
                        <?php include("produtoAdditionalFilePartial.php"); ?>
                      <?php endforeach; ?>

                      <hr>
                      <a href="produtoEdit.php?id=<?php echo $produto['id']; ?>" class="btn btn-info btn-sm">
                        <span class="text">Editar</span>
                      </a>
                      <a href="produtoDelete.php?id=<?php echo $produto['id']; ?>" class="btn btn-danger btn-sm float-right"><span class="text">Deletar</span>
                      </a>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <?php include("templates/footer.php");?>
        </div>
      </div>
    </body>
