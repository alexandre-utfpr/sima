<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';
$requiredFields = ['titulo'];

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields) && ($_FILES['document']['size'] != 0)) {

      if (checkUpload($_FILES["document"], array('pdf'), 5000000)) {
        $uuid = substr(md5(rand()),0,10);
        $target_file = $target_dir . $uuid . '-' . basename($_FILES["document"]["name"]);

        if (move_uploaded_file($_FILES["document"]["tmp_name"], $target_file)) {
          $conn  = OpenCon();

          $titulo = trim($_POST['titulo']);
          $titulo = mysqli_real_escape_string($conn, $titulo);

          $tableName = 'arquivos';
          $fields = 'url';
          $values = "'$target_file'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          $fileId = mysqli_insert_id($conn);

          $tableName = 'legislacoes';
          $fields = 'titulo, arquivo_id';
          $values = "'$titulo', '$fileId'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          if ($queryResult) {
            CloseCon($conn);
            header("Location: adminLegislationIndex.php", true, 301);
          } else {
            $errorMessage = $contactSupport;
            CloseCon($conn);
          }
        } else {
          $errorMessage = $errorUploading;
        }
      } else {
        $errorMessage = $invalidFileError;
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Adicionar legislação'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Adicionar legislação</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="adminLegislationCreate.php" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="titulo" class="control-label required-field">Título</label>
                    <input id="titulo" name="titulo" class="form-control form-control-user"
                    placeholder="Título" type="text" pattern=".{1,100}" required>
                  </div>

                  <hr>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="document" class="control-label required-field">Arquivo (PDF, máx. 5Mb)</label>
                      <input type="file" name="document" id="document">
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Adicionar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
