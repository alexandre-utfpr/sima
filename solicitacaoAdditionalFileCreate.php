<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$pageRestriction = 'admin';
$requiredFields = ['tipo','solicitacao_id'];

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields) && ($_FILES['document']['size'] != 0)) {

      if (checkUpload($_FILES["document"], array('pdf', 'zip'), 5000000)) {
        $uuid = substr(md5(rand()),0,10);
        $target_file = $target_dir . $uuid . '-' . basename($_FILES["document"]["name"]);

        if (move_uploaded_file($_FILES["document"]["tmp_name"], $target_file)) {
          $conn  = OpenCon();

          $tipo = trim($_POST['tipo']);
          $solicitacao_id = trim($_POST['solicitacao_id']);

          $tipo = mysqli_real_escape_string($conn, $tipo);
          $solicitacao_id = mysqli_real_escape_string($conn, $solicitacao_id);

          $tableName = 'arquivos';
          $fields = 'url';
          $values = "'$target_file'";

          $query = InsertQuery($tableName, $fields, $values);
          $queryResult = mysqli_query($conn, $query);

          $fileId = mysqli_insert_id($conn);

          $tableName = 'solicitacoes';
          $fields = "$tipo"."_id = '".$fileId."'";

          $query = UpdateQuery($tableName, $fields, $solicitacao_id);
          $queryResult = mysqli_query($conn, $query);

          if ($queryResult) {
            CloseCon($conn);
            header("Location: solicitacaoUploadedAdditionalDocuments.php?id=".$solicitacao_id, true, 301);
          } else {
            $errorMessage = $contactSupport.mysqli_error($conn).$query;
            CloseCon($conn);
          }
        } else {
          $errorMessage = $errorUploading;
        }
      } else {
        $errorMessage = $invalidFileError;
        header("Location: solicitacaoAdditionalFileCreate.php?id=".$_POST['solicitacao_id']."&tipo=".$_POST['tipo'], true, 301);
        return;
      }
    }
    else {
      $errorMessage = $invalidFileError;
      header("Location: solicitacaoAdditionalFileCreate.php?id=".$_POST['solicitacao_id']."&tipo=".$_POST['tipo'], true, 301);
      return;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Incluir documento adicional'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/".$_SESSION['type']."Sidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/".$_SESSION['type']."Topbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Incluir documento adicional</h6>
              </div>

              <div class="card-body">
                <form id="estabelecimentoCreateForm" data-toggle="validator" role="form" method="post" action="solicitacaoAdditionalFileCreate.php" enctype="multipart/form-data">
                  <input name="tipo" type="hidden" value="<?php echo $_GET['tipo'] ?>">
                  <input name="solicitacao_id" type="hidden" value="<?php echo $_GET['id'] ?>">

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="document" class="control-label required-field">Arquivo (PDF ou ZIP, máx. 5Mb)</label>
                      <input type="file" name="document" id="document">
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Adicionar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
