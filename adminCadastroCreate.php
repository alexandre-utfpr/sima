<?php
require_once 'util/strings.php';
require_once 'util/db_connection.php';
require_once 'util/page_utils.php';

$requiredFields = ['cadastroTipo',
'cadastroNome',
'cadastroCpf',
'cadastroMunicipio',
'cadastroBairro',
'cadastroEndereco',
'cadastroNumero',
'cadastroTelefone',
'cadastroCelular',
'cadastroUf',
'cadastroCep',
'cadastroTelefone'
];

$pageRestriction = 'admin';

if (!ValidSessionAndResource($pageRestriction, null)) {
  header("Location: logout.php", true, 301);
  return;
} else {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (ValidateRequired($_POST, $requiredFields)) {
      $conn  = OpenCon();

      $cadastroTipo = trim($_POST['cadastroTipo']);
      $cadastroNome = trim($_POST['cadastroNome']);
      $cadastroCpf = trim($_POST['cadastroCpf']);
      $cadastroMunicipio = trim($_POST['cadastroMunicipio']);
      $cadastroBairro = trim($_POST['cadastroBairro']);
      $cadastroEndereco = trim($_POST['cadastroEndereco']);
      $cadastroNumero = trim($_POST['cadastroNumero']);
      $cadastroTelefone = trim($_POST['cadastroTelefone']);
      $cadastroCelular = trim($_POST['cadastroCelular']);
      $cadastroUf = trim($_POST['cadastroUf']);
      $cadastroCep = trim($_POST['cadastroCep']);

      $cadastroTipo = mysqli_real_escape_string($conn, $cadastroTipo);
      $cadastroNome = mysqli_real_escape_string($conn, $cadastroNome);
      $cadastroCpf = mysqli_real_escape_string($conn, $cadastroCpf);
      $cadastroMunicipio = mysqli_real_escape_string($conn, $cadastroMunicipio);
      $cadastroBairro = mysqli_real_escape_string($conn, $cadastroBairro);
      $cadastroEndereco = mysqli_real_escape_string($conn, $cadastroEndereco);
      $cadastroNumero = mysqli_real_escape_string($conn, $cadastroNumero);
      $cadastroTelefone = mysqli_real_escape_string($conn, $cadastroTelefone);
      $cadastroCelular = mysqli_real_escape_string($conn, $cadastroCelular);
      $cadastroUf = mysqli_real_escape_string($conn, $cadastroUf);
      $cadastroCep = mysqli_real_escape_string($conn, $cadastroCep);

      $tableName = 'cadastros';
      $fields = 'tipo, nome, cpf, municipio, bairro, endereco, numero, telefone, celular, uf, cep';
      $values = "'$cadastroTipo', '$cadastroNome', '$cadastroCpf', '$cadastroMunicipio', '$cadastroBairro', '$cadastroEndereco', ";
      $values .= "'$cadastroNumero', '$cadastroTelefone', '$cadastroCelular' ,'$cadastroUf', '$cadastroCep'";

      $query = InsertQuery($tableName, $fields, $values);
      $queryResult = mysqli_query($conn, $query);

      if ($queryResult) {
        $id = mysqli_insert_id($conn);

        CloseCon($conn);
        header("Location: adminCadastroShow.php?id=$id", true, 301);
      } else {
        $errorMessage = $contactSupport.mysqli_error($conn).$query;
        CloseCon($conn);
      }
    }
    else {
      $errorMessage = $invalidFieldsError;
    }
  }
}
?>

<!DOCTYPE html>
<html lang="pt-br">
<?php $title = 'Novo Cadastro'; include("templates/header.php");?>

<body id="page-top">
  <div id="wrapper">

    <?php include("templates/adminSidebar.php");?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

        <?php include("templates/adminTopbar.php");?>

        <div class="container-fluid">
          <div class="col-12">
            <div class="card shadow mb-4">
              <?php if(isset($errorMessage)): ?>
                <?php include("templates/error.php");?>
              <?php endif; ?>
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-success">Cadastro de novo produtor</h6>
              </div>

              <div class="card-body">
                <form id="cadastroCreateForm" data-toggle="validator" role="form" method="post" action="adminCadastroCreate.php">

                  <input id="cadastroTipo" name="cadastroTipo" type="hidden" value="produtor">

                  <div class="form-group">
                    <label for="cadastroNome" class="control-label required-field">Nome completo</label>
                    <input id="cadastroNome" name="cadastroNome" class="form-control form-control-user"
                    placeholder="Nome completo" type="text" pattern=".{1,40}" required>
                  </div>

                  <div class="form-group row">
                    <div class="col-3">
                      <label for="cadastroCpf" class="control-label required-field">CPF</label>
                      <input id="cadastroCpf" name="cadastroCpf" class="form-control form-control-user"
                      pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" title="Digite o CPF no formato nnn.nnn.nnn-nn"
                      placeholder="nnn.nnn.nnn-nn" type="text" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-5">
                      <label for="cadastroMunicipio" class="control-label required-field">Municipio</label>
                      <input id="cadastroMunicipio" name="cadastroMunicipio" class="form-control form-control-user"
                      placeholder="Municipio" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-2">
                      <label for="cadastroUf" class="control-label required-field">UF</label>
                      <select name="cadastroUf" class="form-control form-control-user">
                        <?php foreach($ufs as $uf){?>
                          <option value="<?php echo $uf; ?>"><?php echo $uf; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-9">
                      <label for="cadastroEndereco" class="control-label required-field">Endereço</label>
                      <input id="cadastroEndereco" name="cadastroEndereco" class="form-control form-control-user"
                      placeholder="Endereço" type="text" pattern=".{1,80}" required>
                    </div>

                    <div class="col-3">
                      <label for="cadastroNumero" class="control-label required-field">Número</label>
                      <input id="cadastroNumero" name="cadastroNumero" class="form-control form-control-user"
                      placeholder="Número" type="number" min=1 required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-7">
                      <label for="cadastroBairro" class="control-label required-field">Bairro</label>
                      <input id="cadastroBairro" name="cadastroBairro" class="form-control form-control-user"
                      placeholder="Bairro" type="text" pattern=".{1,40}" required>
                    </div>

                    <div class="col-5">
                      <label for="cadastroCep" class="control-label required-field">CEP (Apenas números)</label>
                      <input id="cadastroCep" name="cadastroCep" class="form-control form-control-user"
                      placeholder="CEP (Apenas números)" type="text" pattern="^[0-9]{8}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="cadastroTelefone" class="control-label required-field">Telefone (Com DDD, apenas dígitos)</label>
                      <input id="cadastroTelefone" name="cadastroTelefone" class="form-control form-control-user"
                      placeholder="Telefone (Com DDD, apenas dígitos)" type="text" pattern="^[0-9]{10, 11}" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-4">
                      <label for="cadastroCelular" class="control-label required-field">Celular</label>
                      <input id="cadastroCelular" name="cadastroCelular" class="form-control form-control-user"
                      placeholder="Celular (Com DDD, apenas dígitos)" type="number" pattern="^[0-9]{11}" required>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("templates/footer.php");?>
    </div>
  </div>
</body>
